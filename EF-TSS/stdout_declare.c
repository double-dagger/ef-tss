
void printstate(double [1+N][1+D]);
void printnormalmodes(double [1+Nfree*D],
		      double [1+Nfree*D][1+Nfree*D]);
void printeigensystem_carts(double [1+Nfree*D],
			    double [1+Nfree*D][1+Nfree*D]);
void printsmwcdotgmwc(double [1+Nfree*D]);
void printSdotG_carts(double [1+Nfree*D]);
void printeverything(double [1+N][1+D],
		    double, double [1+Nfree*D],
		    double [1+Nfree*D], 
		    double [1+Nfree*D][1+Nfree*D]);
void printgradient(double [1+Nfree*D]);
void printmwcgradient(double [1+Nfree*D]);
void printhessian(double [1+Nfree*D][1+Nfree*D]);
void accelrysframe(FILE *, double [1+N][1+D]);
void write_movie_filenames(void);

