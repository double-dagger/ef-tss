/*************************************************************************/
/*  EIGENVECTOR FOLLOWING WITH SR1 UPDATES ON HESSIAN CONSTRUCTED FROM   */
/**  POSITIVE DEFINITE PIECE AND ESTIMATED TANGENT & STRING CURVATURE   **/
/*  WHEN 1 IMAGINARY FREQUENCY ALONG FOLLOWING MODE, USES NEWTON-RAPHSON */
/*************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// 
// #ifndef N
// #define N 4
// #define Nfree 2
// #define Charge 0
// #define Spin 1
// #define MEMORY 18		/* past iterations to get follow "mode" */
// #define MAX_DIM_MULTIPLE 10 	/* give up after this many gradients	*/
// #define RESET_HESSIAN 15        /* hessian reset frequency (never = 0)	*/
// #endif


#define D 3
#define DEBUG 0
int Number[1+N];
int Mass[1+N];

/***********************************************************************/
/**********************  Function Declarations  ************************/
/***********************************************************************/

#include "./stdout_declare.c"
#include "./esc_declare.c"
#include "./math_declare.c"
#include "./mwc_cart_declare.c"
#include "./orient_declare.c"
#include "./zmatrix_declare.c"
#include "./ef_declare.c"


void scaninfile(double [1+N][1+D]);
void printoutfile(double [1+N][1+D]);
void print_eigenvalues(double [1+Nfree*D]);
void ef_d2VatxyzB(double [1+N][1+D], 
	double [1+Nfree*D][1+Nfree*D]);
void mode_history_update(double [1+Nfree*D], 
	double [1+MEMORY][1+Nfree*D]);
void mode_history_mean(double [1+MEMORY][1+Nfree*D],
	double [1+Nfree*D]);
void initialize_history(double [1+Nfree*D],
	double [1+MEMORY][1+Nfree*D]);

/*************************************************************************/
/********************************  MAIN  *********************************/
/*************************************************************************/

main(int argc, char *argv[]){

  int i, j, k, nonnull, test, mode_old, mode, count_neg, count;
  int EF_cue, COUNT, kill_count, I, TEST;
  double xyzAmovie[1+N][1+D];
  int j1, k1, followmode;
  double gradsize_for_hessian, crit_min_overlap, min_overlap;
  double xyzA[1+N][1+D], xyzB[1+N][1+D], g[1+Nfree*D];
  double g_old[1+Nfree*D], S[1+Nfree*D][1+Nfree*D];
  double baker[1+Nfree*D], w2[1+Nfree*D], dxyzB[1+N][1+D];
  double hmwc[1+Nfree*D][1+Nfree*D], t[1+Nfree*D], iw2;
  double crit_move, crit_grad, crit_deltaE, V, V_old;
  double Sg[1+Nfree*D], maxsize, lambda, lambdap, guess;
  double deltaV, w2p;
  double Sx[1+Nfree*D], x[1+Nfree*D], gradsize, movesize;
  double rootM[1+N*D], gmwc[1+Nfree*D], overlap, minsize;
  double h[1+Nfree*D][1+Nfree*D], t_hist[1+MEMORY][1+Nfree*D];
  double Sxp, alpha, Sxnotp, SxEF[1+Nfree*D], SxNR[1+Nfree*D];
  double SgEF[1+Nfree*D], SgNR[1+Nfree*D], SxEFnorm, SxNRnorm;
  FILE *outfile, *graphicsfile, *countgrads, *convergence;
  FILE *infile, *hessianfile, *tsfile;

  scaninfile(xyzA); 

  convergence = fopen("convergence.txt","w");
  fprintf(convergence,
  	"grads |grad| deltaV movesize overlap Y/N");
  graphicsfile = fopen("ef_movie.xyz","w");

  build_rootM(Mass, rootM);
  fflush(stdout);
  if(N == Nfree  &&  N!=1){
    nonnull = Nfree*D-6;
  }else{
    nonnull = Nfree*D;
  }
  count = 0;
  COUNT = 0;
  crit_move = .003;
  crit_grad = .0003; 
  crit_deltaE = .00001;
  crit_min_overlap = 0.707;
  gradsize_for_hessian = 0.1;
  maxsize = 0.08;
  minsize = 0.707*maxsize;
  printf("\n  MAXIMUM STEP SIZE ALL STEPS (BOHR AMU.5)=%f", maxsize);
  printf("\n  MINIMUM SIZE STEPS WITH WRONG STRUCTURE =%f", minsize);

  angstromtobohr(xyzA, xyzB);

  fflush(stdout);

  printf("\n  EIGENVECTOR FOLLOWING FROM GEOMETRY & STARTING HESSIAN");
  countgrads = fopen("gradjobcount.txt","w");
  fprintf(countgrads,"%d ",count);
  fclose(countgrads);

      /****************************************************************/
      /*************  EIGENVECTOR FOLLOWING ITERATIONS  ***************/
      /****************************************************************/
      
  kill_count = 0;
  test = 0;
  for(i=1;test == 0;i=i+1){

     if(N == Nfree && N != 1){
       center_of_mass(xyzB);
     }
 	
     if(i != 1){
       mode_old = mode;
       V_old = V;
       for(j=1;j<=Nfree*D;j=j+1){
	 Sg[j] = 0.0;
	 Sx[j] = 0.0;
	 g_old[j] = gmwc[j];
       }
     }
     printf("\n\n  STATE %d -------------------ANGSTROMS",i);
     bohrtoangstrom(xyzB, xyzA);
     accelrysframe(graphicsfile, xyzA);
     fflush(graphicsfile);
     printstate(xyzA);
    
     V = VdVatxyzB(xyzB, g);
     printgradient(g);
     gcarttomwc(g, gmwc, rootM);
     gradsize = sqrt(dotNfreeD(gmwc,gmwc));
      
        /*************************************************************/
        /******************  UPDATE/RESET HESSIAN  *******************/
        /*************************************************************/

     if(i == 1){
        printf("\n  SET HESSIAN TO TRUE HESSIAN");
        V = VdVd2VatxyzB(xyzB, g, h);
	printhessian(h);
      	hcarttomwc(h, hmwc, rootM);
	fprintf(convergence,"\n");
	//sleep(1000);


     }else{
        SR1_update(xyzB, x, gmwc, g_old, hmwc);
          
	if(RESET_HESSIAN != 0  &&  i%RESET_HESSIAN == 1){        
          printf("\n  RESET HESSIAN TO TRUE HESSIAN");
      	  V = VdVd2VatxyzB(xyzB, g, h);
	  
      	  hcarttomwc(h, hmwc, rootM);
	  fprintf(convergence,"\n");
        }
     }
     normal_mode_analysis(xyzB, w2, S, hmwc);
     printf("\n\n  EIGENVALUES ");    
     print_eigenvalues(w2);
     fflush(stdout);

     count = count_grad_jobs();
     deltaV = V - V_old;

        /**************************************************************/
        /*******************  PICK A MODE TO FOLLOW *******************/
        /**************************************************************/

     if(i == 1){
       k = 1;
       for(j=1;j<=Nfree*D;j=j+1){
	 if(w2[j] < w2[k]){
	   k = j;
         }
       }
       for(j=1;j<=Nfree*D;j=j+1){
	 t[j] = S[k][j];
       }
       initialize_history(t, t_hist);
     }
     mode_history_mean(t_hist, baker);
     count_neg = 0;
     mode = 1;
     overlap = fabs(dotNfreeD(baker, S[1]));
     for(j=1;j<=nonnull;j=j+1){
       Sg[j] = dotNfreeD(S[j], gmwc);
       if( fabs(dotNfreeD(baker, S[j])) > overlap ){
	 mode = j;
	 overlap = fabs(dotNfreeD(baker, S[j]));
       }
       if(w2[j] < 0.0){
	 count_neg = count_neg + 1;
       }
     }
        
	printf("\n\n  HESSIAN HAS %d IMAGINARY FREQUENCIES", count_neg);
        printf("\n  FOLLOWING MODE %d WITH FREQUENCY %f", mode, w2[mode]);
        printf("\n  ABSOLUTE OVERLAP WITH MODE HISTORY %.3f", overlap);
        fflush(stdout);

     if(dotNfreeD(baker, S[mode]) < 0.0){
	for(j=1;j<=Nfree*D;j=j+1){
	  S[mode][j] = -S[mode][j];
        } 
        Sg[mode] = -Sg[mode];
     }
     mode_history_update(S[mode], t_hist);


	/***********************************************************/
	     printf("\n\n  ANIMATE MODE BEING FOLLOWED IN MOVIE");
 	/***********************************************************/

 
        for(j1=1;j1<=N;j1=j1+1){
	  for(k1=1;k1<=D;k1=k1+1){
	    xyzAmovie[j1][k1] = xyzA[j1][k1];
          }
        }
        for(j=0;j<=8;j=j+1){
          for(j1=1;j1<=Nfree;j1=j1+1){
            for(k1=1;k1<=D;k1=k1+1){
	      xyzAmovie[j1][k1] = xyzA[j1][k1]+
			sin(3.1416*(double)j/4.0)*S[mode][(j1-1)*D+k1];
	    }
	  }
          accelrysframe(graphicsfile,xyzAmovie);
        }

        /***********************************************************/
        /*****  DECIDE WHAT KIND OF (MIXTURE OF) STEP TO TAKE  *****/
        /***********************************************************/
     
     EF_cue = 0;
     for(j=1;j<=Nfree*D;j=j+1){
  	SxEF[j] = SxNR[j] = SgEF[j] = SgNR[j] = Sx[j] = x[j] = 0.0;
     }
     for(k=1;k<=nonnull;k=k+1){
	Sg[k] = dotNfreeD(S[k], gmwc);
	if(w2[k] > 0.0){
	  SxNR[k] = -Sg[k]/w2[k];
        }else{
	  SxEF[k] = -Sg[k]*minsize/fabs(Sg[k]);
 	  if(k != mode){
	    EF_cue = EF_cue + 1;
	  }
        }
     }
     if(w2[mode] < 0.0){
	SxNR[mode] = -Sg[mode]/w2[mode];
 	SxEF[mode] = 0.0;
     }else{
	SxNR[mode] = 0.0;
	SxEF[mode] = Sg[mode]*minsize/fabs(Sg[mode]);
	EF_cue = EF_cue + 1;
     }

     SxEFnorm = sqrt(dotNfreeD(SxEF, SxEF));
     SxNRnorm = sqrt(dotNfreeD(SxNR, SxNR));
     alpha = SxEFnorm*SxEFnorm + SxNRnorm*SxNRnorm;

     printf("\n  NR PORTION NORM:%f    EF PORTION NORM:%f   TOTAL:%f",
  	  	SxNRnorm, SxEFnorm, sqrt(alpha));

        /**********************************************************/
        /*******  STEPSIZE CONTROL: NEVER EXPAND NR PART  *********/
        /**********************************************************/

     if(alpha < minsize*minsize  &&  EF_cue != 0){
       alpha = sqrt(minsize*minsize-SxNRnorm*SxNRnorm)/SxEFnorm;
       for(k=1;k<=nonnull;k=k+1){
  	 SxEF[k] = alpha*SxEF[k];
       }
       printf("\n  SMALLER THAN MINSIZE.");
       printf("\n  SCALING EF PORTION OF STEP BY %f", alpha);
     }
     if(alpha > maxsize*maxsize  &&  EF_cue != 0){
       if( SxEFnorm*SxEFnorm > minsize*minsize ){
	 for(k=1;k<=nonnull;k=k+1){
	   SxEF[k] = SxEF[k]*minsize/SxEFnorm;
  	 }
       }
       if( SxNRnorm*SxNRnorm > minsize*minsize ){
         for(k=1;k<=nonnull;k=k+1){
           SxNR[k] = SxNR[k]*minsize/SxNRnorm;
	 }
       }
     }

     for(k=1;k<=nonnull;k=k+1){
       Sx[k] = SxEF[k] + SxNR[k];
     }
     alpha = sqrt(dotNfreeD(Sx, Sx));
     if(alpha > maxsize){
       printf("\n  STEPSIZE %.3f TOO BIG - UNIFORMLY SCALE TO %.3f",
	    alpha, maxsize);
       for(k=1;k<=nonnull;k=k+1){
	 Sx[k] = Sx[k]*maxsize/alpha;
       }
     }
     for(j=1;j<=Nfree*D;j=j+1){
       for(k=1;k<=nonnull;k=k+1){
	 x[j] = x[j] + S[k][j]*Sx[k];
       }
     }
     printf("\n  STEP IN EIGENVECTOR BASIS");
     print_eigenvalues(Sx);
     
     movesize = sqrt(dotNfreeD(x, x));
     fprintf(convergence,"\n %d %f %f %f %f %d %f", 
	     count-COUNT, gradsize, 
	     deltaV, movesize, overlap, count_neg, V);
     if(w2[mode] < 0.0){
       fprintf(convergence," Y");
       if(kill_count > 0){
	 kill_count = 0;
       }
       min_overlap = overlap;
     }else{
       fprintf(convergence," N");
       kill_count = kill_count + 1;
       if(overlap < min_overlap){
	 min_overlap = overlap;
	 fprintf(convergence," min_overlap=%f",min_overlap);
       }   /*  TRACKS ERROR IN EIGENVECTOR FOLLOWING  */
     }
     fflush(convergence);
     
     for(j=1;j<=Nfree;j=j+1){
       for(k=1;k<=D;k=k+1){
	 dxyzB[j][k] = x[(j-1)*D+k]/sqrt(Mass[j]);
	 xyzB[j][k] = xyzB[j][k] + dxyzB[j][k];
       }
     } 
     center_of_mass(xyzB);
     
     /*************************************************************/
     /*******************  CONVERGENCE TESTING  *******************/
     /*************************************************************/
     
     printf("\n  CONVERGENCE TESTING");
     printf("\n   movesize:tolerance  %f::%f", movesize,crit_move);
     printf("\n   gradsize:tolerance  %f::%f", gradsize,crit_grad);
     printf("\n   deltaE:|tolerance|  %f::%f", deltaV,crit_deltaE);
        
     if(count_neg == 1  &&  w2[mode] < 0.0){
       if(movesize < crit_move && gradsize < crit_grad){
	 test = 1;
       }
       if(fabs(deltaV) < crit_deltaE && gradsize < crit_grad){
	 test = 1;
       }
       if(test == 1){
	 printf("\n  CONVERGED TO SADDLE POINT ");
	 fprintf(convergence,"\n  CONVERGED");
       }  
     }
     if(test == 0){
       if( i > MAX_DIM_MULTIPLE*nonnull ){
	 printf("\n  ITERATIONS EXCEEDED %d TIMES DIMENSIONALITY", 
		MAX_DIM_MULTIPLE);
	 printf("\n  EF ALGORITHM KILLED");
	 test = 2;
	 fprintf(convergence,"\n  MAX ITERATIONS EXCEEDED");
       }
     }
     if(count_neg != 1  ||  w2[mode] > 0.0){
          printf("\n  HESSIAN HAS INCORRECT STRUCTURE - NOT CONVERGED");
     }
     if((((double)kill_count) > (3.0/minsize))
	&& (min_overlap < crit_min_overlap)){
       test = 2;
     }
     if(test == 2){
       fprintf(convergence,"\n  KILLED\n");
     }
     fflush(graphicsfile);
     fflush(convergence);
     
     printf("\n  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
  }
      
  fclose(graphicsfile);
  fclose(convergence);

  graphicsfile = fopen("tsfile.xyz","w");
  bohrtoangstrom(xyzB, xyzA);
  accelrysframe(graphicsfile, xyzA);
  fflush(graphicsfile);
  fclose(graphicsfile);

  fclose(infile);
  printf("\n\n  BYE BYE");
}


/*************************************************************************/
/****************************   SUBROUTINES  *****************************/
/*************************************************************************/

#include "./stdout_define.c"
#include "./esc_define.c"
#include "./math_define.c"
#include "./mwc_cart_define.c"
#include "./orient_define.c"
#include "./zmatrix_define.c"
#include "./ef_define.c"


void printoutfile(double xyzA[1+N][1+D]){
  FILE *outfile;
  int i,j;
  double temp;
  
  outfile = fopen("inflection###.txt","a");
  /**  IN CASE I FORGET TO CLEAR IT  **/
  for(i=1;i<=N;i=i+1){
    fprintf(outfile,"%d ", Number[i]);
  }
  for(i=1;i<=N;i=i+1){
    fprintf(outfile,"%d ", Mass[i]);
  }
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      fprintf(outfile,"%lf ", xyzA[i][j]);
    }
  }
}

void print_eigenvalues(double w2[1+Nfree*D]){
  int i;
  printf("\n");
  for(i=1;i<=Nfree*D;i=i+1){
    printf("  %f", w2[i]);
    if(i%6 == 0){
      printf("\n");
    }
  }
  fflush(stdout);
}

void mode_history_update(double t_new[1+Nfree*D], 
	double t_hist[1+MEMORY][1+Nfree*D]){
  int i, j, k;
  
  for(i=MEMORY-1;i>=1;i=i-1){
    for(j=1;j<=Nfree*D;j=j+1){
      t_hist[i+1][j] = t_hist[i][j];
    }
  }
  for(j=1;j<=Nfree*D;j=j+1){
    t_hist[1][j] = t_new[j];
  }
}

void mode_history_mean(double t_hist[1+MEMORY][1+Nfree*D],
	double t_mean[1+Nfree*D]){
  int i, j;
  
  for(i=1;i<=Nfree*D;i=i+1){
    t_mean[i] = 0.0;
  }
  for(i=1;i<=MEMORY;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      t_mean[j] = t_hist[i][j] + t_mean[j];
    }
  }
  normalizeNfreeD(t_mean);
}

void initialize_history(double t[1+Nfree*D],
			double t_hist[1+MEMORY][1+Nfree*D]){
  
  int i, j;
  for(i=1;i<=MEMORY;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      t_hist[i][j] = t[j];
    }
  }
}

void scaninfile(double xyzA[1+N][1+D]){
  FILE *infile;
  int i,j;
  double temp;
  
  infile = fopen("nearsaddleA","r");
  
  for(i=1;i<=N;i=i+1){
    fscanf(infile,"%d ", &j);
    Number[i] = j;
  }
  for(i=1;i<=N;i=i+1){
    fscanf(infile,"%d ", &j);
    Mass[i] = j;
  }
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      fscanf(infile,"%lf ",&temp);
      xyzA[i][j] = temp;
    }
  }
}

