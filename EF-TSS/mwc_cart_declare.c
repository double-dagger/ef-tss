void build_rootM(int [1+N], double [1+N*D]);
void xyzmwctocart(double [1+N][1+D], double [1+N][1+D],
		  double [1+N*D]);
void xyzcarttomwc(double [1+N][1+D], double [1+N][1+D],
		  double [1+N*D]);

void NDx1carttomwc(double [1+N*D], double [1+N*D], 
		   double [1+N*D]);
void NfreeDx1carttomwc(double [1+Nfree*D], double [1+Nfree*D], 
		   double [1+N*D]);
void NDx1mwctocart(double [1+N*D], double [1+N*D], 
		   double [1+N*D]);
void NfreeDx1mwctocart(double [1+Nfree*D], double [1+Nfree*D], 
		   double [1+N*D]);

void Hmwctocart(double [1+N*D][1+N*D],
		double [1+N*D][1+N*D], double [1+N*D]);
void hmwctocart(double [1+Nfree*D][1+Nfree*D],
		double [1+Nfree*D][1+Nfree*D], double [1+N*D]);
void Hcarttomwc(double [1+N*D][1+N*D],
		double [1+N*D][1+N*D], double [1+N*D]);
void hcarttomwc(double [1+Nfree*D][1+Nfree*D],
		double [1+Nfree*D][1+Nfree*D], double [1+N*D]);

void Gcarttomwc(double [1+N*D], double [1+N*D], double [1+N*D]);
void Gmwctocart(double [1+N*D], double [1+N*D], double [1+N*D]);
void gcarttomwc(double [1+Nfree*D], double [1+Nfree*D], double [1+N*D]);
void gmwctocart(double [1+Nfree*D], double [1+Nfree*D], double [1+N*D]);

void NxDtoNDx1(double [1+N][1+D], double [1+N*D]);
void NDx1toNxD(double [1+N*D], double [1+N][1+D]);
void NfreexDtoNfreeDx1(double [1+Nfree][1+D], double [1+Nfree*D]);
void NfreeDx1toNfreexD(double [1+Nfree*D], double [1+Nfree][1+D]);

