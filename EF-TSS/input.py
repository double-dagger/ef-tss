import json
import subprocess
from distutils.dir_util import copy_tree
import shutil
import os

def bash_command(cmd):
    """
    Executes a bash command. Waits until the process finishes before continuing

    input:
    cmd - str, a string of the bash command to execute
    """
    process = subprocess.Popen(cmd, shell=True, executable='/bin/bash')
    process.wait()
    pass

def make_remdV(nproc, link0_kwds, route_card, wrk_dir):
    """
    Creates the file `remdV`, which contains link 0 and route card information
    for a force calculation

    inputs
    nproc - int, number of processors to use
    link0_kwds - list of str, any additional link 0 commands to use
    route_card - route card options IN ADDITION to the force calc options
                 `#p FORCE NOSYMM SCF=(MaxCycle=1000) `. This will include things like
                 your density functional/basis set
    wrk_dir - directory where the input files are, will write file to same place
    """
    remdV = []
    remdV.append('%nproc={}'.format(nproc))
    for x in link0_kwds:
        remdV.append(x)
    remdV.append('#p FORCE NOSYMM SCF=(MaxCycle=1000) '+route_card)
    remdV.append('')
    remdV.append('FORCE CALCULATION')
    remdV.append('')
    with open(wrk_dir+'/remdV', 'w') as f:
        for x in remdV:
            f.write(x+'\n')
    pass

def make_remd2V(nproc, link0_kwds, route_card, wrk_dir):
    """
    Creates the file `remd2V`, which contains link 0 and route card information
    for a Hessian calculation

    inputs
    nproc - int, number of processors to use
    link0_kwds - list of str, any additional link 0 commands to use
    route_card - route card options IN ADDITION to the force calc options
                 `#p FREQ NOSYMM PUNCH(DERIVATIVES) SCF=(MaxCycle=1000) `. This will include things like
                 your density functional/basis set
    wrk_dir - directory where the input files are, will write file to same place
    """
    remd2V = []
    remd2V.append('%nproc={}'.format(nproc))
    for x in link0_kwds:
        remd2V.append(x)
    remd2V.append("#p FREQ NOSYMM PUNCH(DERIVATIVES) SCF=(MaxCycle=1000) "+route_card)
    remd2V.append('')
    remd2V.append('FORCE CALCULATION')
    remd2V.append('')
    with open(wrk_dir+'/remd2V', 'w') as f:
        for x in remd2V:
            f.write(x+'\n')
    pass

# Setting up some directories, will need to fix later
code_dir = os.environ.get('EFTSS_DIR')
base_dir = os.getcwd()
wrk_dir = os.getcwd()

print(base_dir)

# Load the input file
input_file = wrk_dir + '/input.json'
with open(input_file) as f:
    input_dict = json.load(f)


# Copy code to test directory
copy_tree(code_dir, wrk_dir+'/temp')

# steps 4-5: compile bpef_from_hess
# The actual command invokes a bunch of macros 
# Originally you had to make a copy of bpef_from_hess, manually change the 
compile_cmd = 'gcc -lm -DN={} -DNfree={} -DCharge={} -DSpin={} -DMEMORY={} -DMAX_DIM_MULTIPLE={} -DRESET_HESSIAN={}'.format(
                  input_dict['N'], input_dict['Nfree'], input_dict['Charge'], input_dict['Spin'], input_dict['MEMORY'],
                  input_dict['MAX_DIM_MULTIPLE'], input_dict['RESET_HESSIAN'])

compile_cmd +=  ' -o {}/bpef_from_hess.out '.format(base_dir) + code_dir + '/bpef_from_hess.c'

print('Compiling c files with parameters supplied by input.json ...')
print('GCC compile command: ', compile_cmd)
bash_command(compile_cmd)

# Step 1-3: run ./make to set up nearsaddleA (thanks for making that script, Xijun)
input_make_cmd = 'sh ' + base_dir+ '/temp/make.sh ' + wrk_dir
bash_command(input_make_cmd)

# Everything is compiled, now c files can be deleted
shutil.rmtree(wrk_dir+'/temp')

# Create remdV and remd2V files
make_remdV(input_dict['nprocs'], input_dict['link-0'], input_dict['route-append'], wrk_dir)
make_remd2V(input_dict['nprocs'], input_dict['link-0'], input_dict['route-append'], wrk_dir)

# Slurm submission script
submit_cmd = 'sh {}/subTSS.sh {} {} {}'.format(code_dir, input_dict['name'], input_dict['nprocs'], wrk_dir)
bash_command(submit_cmd)