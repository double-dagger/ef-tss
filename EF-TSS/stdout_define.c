
/*************************************************************************/
/***************  prints state of system to the screen   *****************/
/*************************************************************************/

void printstate(double xyz[1+N][1+D] ){
  int j,k;
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};
  if(D == 3){
    printf("\n      x        y        z");
    printf("\n  ___________________________");
  }
  for(j=1;j<=N;j=j+1){
    printf("\n  %s",symbol[Number[j]]);
    for(k=1;k<=D;k=k+1){
      printf("  %.5f",xyz[j][k]);
      if(j == Nfree && k == D){
	printf("\n");
      }
    }
  }
  printf("\n");
}

void printnormalmodes(double d[1+Nfree*D],
		      double S[1+Nfree*D][1+Nfree*D]){
  int j,k,nmax;
  double cminverse[1+Nfree*D];
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};


  /* Max you can see is 7 atoms, so just print 1st Seven */

  if(Nfree >= 6){
    nmax = 6;
  }
  else{
    nmax = Nfree;
  }
  printf("\n\n  INTERNAL NORMAL MODES:");
  printf("\n  frequencies v/(cm^-1):\n  ");
  for(j=1;j<=Nfree*D;j=j+1){
    if(j <= N*D-6){
      if(d[j] > 0.0){
	cminverse[j] = sqrt(d[j])/.0001953;
	printf(" %.1f ",cminverse[j]);
      }
      else{
	cminverse[j] = sqrt(-d[j])/.0001953;
	printf(" %.1f(i) ",cminverse[j]);
      }
    }  
    if(j%6 == 0){
      printf("\n  ");
    }
  }
  printf("\n\n  w^2/(H/(a0^2 amu)), HmwcEigenvector/(a0)");
  printf("\n  __________________________________________\n          ");
  for(j=1;j<=nmax;j=j+1){
    printf(" %s: x    y    z ",symbol[Number[j]]);
  }
  for(j=1;j<=D*Nfree;j=j+1){
    printf("\n  %.5f  ", d[j]);
    for(k=1;k<=nmax*D;k=k+1){
      if(S[j][k] >= 0.0){
	printf(" ");
      }
      if(k%D == 1){
	printf(" ");
      }
      printf("%.1f ",S[j][k]);
    }
  }
  printf("\n");
}

void printeigensystem_carts(double d[1+Nfree*D],
			    double S[1+Nfree*D][1+Nfree*D]){
  int j,k,nmax;
  double cminverse[1+Nfree*D];
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};


  /* Max you can see is 7 atoms, so just print 1st Seven */

  if(Nfree >= 6){
    nmax = 6;
  }
  else{
    nmax = Nfree;
  }
  printf("\n\n  d2V EIGENSYSTEM - SPRING CONSTANTS - NOT VIBRATIONAL MODES:");
  printf("\n\n  k/(H/(a0^2)), 2ndMatrix Eigenvector/(a0)");
  printf("\n  __________________________________________\n          ");
  for(j=1;j<=nmax;j=j+1){
    printf(" %s: x    y    z ",symbol[Number[j]]);
  }
  for(j=1;j<=D*Nfree;j=j+1){
    printf("\n  %.5f  ", d[j]);
    for(k=1;k<=nmax*D;k=k+1){
      if(S[j][k] >= 0.0){
	printf(" ");
      }
      if(k%D == 1){
	printf(" ");
      }
      printf("%.1f ",S[j][k]);
    }
  }
  printf("\n");
}

void printsmwcdotgmwc(double sdotg[1+Nfree*D]){
  int j;
  printf("\n  MWCGRADIENT IN MWCEIGENVECTOR BASIS:\n  ");
  for(j=1;j<=Nfree*D;j=j+1){
    printf("%.4f ", sdotg[j]);
    if(j%6 == 0){
      printf("\n  ");
    }
  }
  printf("\n  NORM OF MWCGRADIENT: %.4f",
	 sqrt(dotNfreeD(sdotg,sdotg))); 
}

void printSdotG_carts(double sdotg[1+Nfree*D]){
  int j;
  printf("\n  GRADIENT IN EIGENVECTOR BASIS:\n  ");
  for(j=1;j<=Nfree*D;j=j+1){
    printf("%.4f ", sdotg[j]);
    if(j%6 == 0){
      printf("\n  ");
    }
  }
  printf("\n  NORM OF GRADIENT: %.4f",
	 sqrt(dotNfreeD(sdotg,sdotg))); 
}
    
void printeverything(double xyz[1+N][1+D],
		    double V, double G[1+Nfree*D],
		    double w2[1+Nfree*D], 
		    double Smwcncarts[1+Nfree*D][1+Nfree*D]){

  printf("\n  ___________________________________________________________");
  printstate(xyz);
  printf("\n  V/(Hartree) = %f\n",V);
  printgradient(G);
  printnormalmodes(w2,Smwcncarts);
  printf("\n  1%s\n","Hartree/(bohr amu^.5)^2 = 9.372*10^29/sec^2");
}

void printgradient(double G[1+Nfree*D]){
  int j,k;
  printf("\n  Gradient/(Hartree/Bohr):");
  printf("\n  _______________________");
  for(j=1;j<=Nfree;j=j+1){
    printf("\n");
    for(k=1;k<=D;k=k+1){
      printf("  %.3f", G[(j-1)*D+k]);
    }
  }
  printf("\n");
}

void printmwcgradient(double G[1+Nfree*D]){
  int j,k;
  printf("\n  Gradient/(Hartree/Bohr amu1/2):");
  printf("\n  _______________________");
  for(j=1;j<=Nfree;j=j+1){
    printf("\n");
    for(k=1;k<=D;k=k+1){
      printf("  %.3f", G[(j-1)*D+k]);
    }
  }
  printf("\n");
}

void printhessian(double H[1+Nfree*D][1+Nfree*D]){
  int j,k;
  printf("\n  Hessian/(Hartree/Bohr^2):");
  printf("  _________________________");
  for(j=1;j<=Nfree*D;j=j+1){
    printf("\n ");
    for(k=1;k<=Nfree*D;k=k+1){
      if(k%3 == 1){
	printf(" ");
      }
      printf(" %.2f ",H[j][k]);
    }
  }
  printf("\n");
}

void accelrysframe(FILE *outfile, double xyzA[1+N][1+D]){
  int i,j,k;
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};

  fprintf(outfile,"%d\n",N);
  fprintf(outfile,"comments\n");
  for(j=1;j<=N;j=j+1){
    fprintf(outfile,"%s %f %f %f\n",
	    symbol[Number[j]],
	    xyzA[j][1],xyzA[j][2],xyzA[j][3]);
  } 
}

void write_movie_filenames(void){
  FILE *namestorage;
  int i;
  namestorage = fopen("framenames","w");
  for(i=1;i<=9998;i=i+1){
    fprintf(namestorage,"%.4d.xyz",i);
  }
  fclose(namestorage);
}

