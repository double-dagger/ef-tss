
double lambdan(int mode, double w2[1+Nfree*D], 
		double Sg[1+Nfree*D]){

  double guess, one_term_lambda[1+Nfree*D];
  double lmax, lmin, temp, num, lmax1;

  int i, min_index, test, count_neg;
  count_neg = 0;
  for(i=1;i<=Nfree*D;i=i+1){
    if(w2[i] < 0.0  &&  i != mode){
      count_neg = count_neg + 1;
    }
    one_term_lambda[i] = 
	w2[i]/2.0 - sqrt(w2[i]*w2[i]+4.0*Sg[i]*Sg[i])/2.0;
  }
  min_index = smallest_excluding_kth(mode, one_term_lambda);

  lmax = lmax1 = one_term_lambda[min_index];
  printf("\n  smallest one term solution: %f",lmax);
  if(count_neg > 1){
    lmin = baker_f(mode, lmax, w2, Sg);
    printf("\n  lower bound to eigenvalue : %f",lmin);
    temp = 1.0-baker_deriv(mode, lmax, w2, Sg);
    lmax = (lmin - baker_deriv(mode, lmax, w2, Sg)*lmax)/temp;
    printf("\n  upper bound to eigenvalue : %f",lmax);
	
    temp = lmax;
    for(i=1;test == 0;i=i+1){
    num = (baker_f(mode, temp, w2, Sg)-temp);
    num = num/(baker_deriv(mode, temp, w2, Sg)-1.0);
    temp = temp - num;

    if(num < 0.0001*(lmax-lmin)){
      test = 1;
    }
    if(i > 10){
      test = 1;
      printf("\n  MAX ITERATIONS IN LAMBDAN ROUTINE");
    }
    }
    if(temp > lmax1  ||  temp < lmin){
	temp = -sqrt(lmin*lmax1);
    }
  }else{
    temp = lmax;
  }
  printf("\n  approximate eigenvalue: %f", temp);
  fflush(stdout);
  return temp;
}

double baker_f(int mode, double lambda,
		double w2[1+Nfree*D], double Sg[1+Nfree*D]){

	int i;
	double temp;

	temp = 0.0;
	for(i=1;i<=Nfree*D;i=i+1){
	  if(i != mode  &&  w2[i] < 0.0){
   	    temp = temp + Sg[i]*Sg[i]/(lambda - w2[i]);
	  }
	}
	return temp;
}

double baker_deriv(int mode, double lambda,
		double w2[1+Nfree*D], double Sg[1+Nfree*D]){

	int i;
	double temp_sum, temp;

	temp_sum = 0.0;
	for(i=1;i<=Nfree*D;i=i+1){
	  if(i != mode  &&  w2[i] < 0.0){
	    temp = Sg[i]/(lambda - w2[i]);
	    temp_sum = temp_sum - temp*temp;
	  }
	}
	return temp_sum;
}


int smallest_excluding_kth(int k, double x[1+Nfree*D]){
	
	int i, min_index;
	double temp;

	temp = x[1];
	min_index = 1;
	if(1 == k){
		temp = x[2];
		min_index = 2;
	}
	for(i=1;i<=Nfree*D;i=i+1){
		if(i == k){ 
		}else{
			if(x[i] < x[min_index]){
				min_index = i;
			}
		}
	}
	return min_index;
}

int largest_excluding_kth(int k, double x[1+Nfree*D]){
	
	int i, min_index;
	double temp;

	temp = x[1];
	min_index = 1;
	if(1 == k){
		temp = x[2];
		min_index = 2;
	}
	for(i=1;i<=Nfree*D;i=i+1){
		if(i == k){ 
		}else{
			if(x[i] > x[min_index]){
				min_index = i;
			}
		}
	}
	return min_index;
}

