
/*************************************************************************/
/*************************  Math Utilities  ******************************/
/*************************************************************************/

void bohrtoangstrom(double xyz1[1+N][1+D],double  xyz2[1+N][1+D]){
  /* can choose to overwrite old coords with new  */
  /* also scales the mass weighted cs to bohrs    */
  int j,k;
  double convert = .5292;

  for(j=1;j<=N;j=j+1){
    for(k=1;k<=D;k=k+1){
      xyz2[j][k] = convert * xyz1[j][k];
    }
  }
}

void angstromtobohr(double xyz1[1+N][1+D], double xyz2[1+N][1+D]){
  /* can choose to overwrite old coords with new  */
  /* also scales the mass weighted cs to bohrs    */
  int j,k;
  double convert = .5292;
  /* 1 bohr = .5292 Angstrom */
  for(j=1;j<=N;j=j+1){
    for(k=1;k<=D;k=k+1){
      xyz2[j][k] = xyz1[j][k]/convert;
    }
  }
}

// int round(double x){
//   int X;
//   double xf;
//   xf = floor(x);
//   if((x-xf) < 0.5){
//     X = (int)xf;
//   }else{
//     X = (int)xf + 1;
//   }
//   return X;
// }

double det2x2(double A[1+2][1+2]){
   
   double temp;
   temp = A[1][1]*A[2][2]-A[1][2]*A[2][1];
   return temp;
 }

double det3x3(double A[1+3][1+3]){
  int i,j,k;
  double a[1+2][1+2];
  double temp, m;
  
  /**  EXPAND ON TOP ROW  **/
  
  temp = 0.0;
  for(i=1;i<=3;i=i+1){
    m = ((double)(2*(i%2)-1));
    for(k=1;k<=3;k=k+1){
      for(j=2;j<=3;j=j+1){
	if(k < i){
	  a[j-1][k] = A[j][k];
	}
	if(k > i){
	  a[j-1][k-1] = A[j][k];
	}
      }
    }
    temp = temp + A[1][i]*m*det2x2(a);
  }
  if(DEBUG == 1){
	printf(" det3x3=%.4f",temp);
  }
  return temp;
}

double det4x4(double A[1+4][1+4]){

  int i,j,k;
  double a[1+3][1+3];
  double temp, m;
  
  temp = 0.0;
  for(i=1;i<=4;i=i+1){
    m = ((double)(2*(i%2)-1));
    for(k=1;k<=4;k=k+1){
      for(j=1;j<=3;j=j+1){
	if(k < i){
	  a[j][k] = A[j+1][k];
	}
	if(k > i){
	  a[j][k-1] = A[j+1][k];
	}
      }
    }
    temp = temp + A[1][i]*m*det3x3(a);
  }
  return temp;
}

void adjoint3x3(double A[1+3][1+3], double Aadj[1+3][1+3]){
  int j,k,i,l;
  double a[1+2][1+2];
  double m, temp;
  
  for(i=1;i<=3;i=i+1){
    for(l=1;l<=3;l=l+1){
      m = ((double)(2*((i+l+1)%2) - 1));
      for(j=1;j<=3;j=j+1){
	for(k=1;k<=3;k=k+1){
	  if(j < i){
	    if(k < l){
	      a[j][k] = A[j][k];
	    }
	    if(k > l){
	      a[j][k-1] = A[j][k];
	    }
	  }
	  if(j > i){
	    if(k < l){
	      a[j-1][k] = A[j][k];
	    }
	    if(k > l){
	      a[j-1][k-1] = A[j][k];
	    }
	  }
	}
      }
      Aadj[l][i] = m*det2x2(a);
    }
  }
}

void adjoint4x4(double A[1+4][1+4], double Aadj[1+4][1+4]){
  int j,k,i,l;
  double a[1+3][1+3];
  double m, temp;
  
  for(i=1;i<=4;i=i+1){
    for(l=1;l<=4;l=l+1){
      m = ((double)(2*((i+l+1)%2) - 1));
      for(j=1;j<=4;j=j+1){
	for(k=1;k<=4;k=k+1){
	  if(j < i){
	    if(k < l){
	      a[j][k] = A[j][k];
	    }
	    if(k > l){
	      a[j][k-1] = A[j][k];
	    }
	  }
	  if(j > i){
	    if(k < l){
	      a[j-1][k] = A[j][k];
	    }
	    if(k > l){
	      a[j-1][k-1] = A[j][k];
	    }
	  }
	}
      }
      Aadj[l][i] = m*det3x3(a);
    }
  }
}


void invert3x3(double M1[1+3][1+3]){  
  double Adj[1+3][1+3];        
  double temp;
  int j,k;

  adjoint3x3(M1, Adj);
  
  temp = det3x3(M1);
  
  for(j=1;j<=3;j=j+1){
    for(k=1;k<=3;k=k+1){
      M1[j][k] = Adj[j][k]/temp;
    }
  }
}

void invert4x4(double M1[1+4][1+4]){  
  double Adj[1+4][1+4];        
  double temp;
  int j,k;

  adjoint4x4(M1, Adj);
  
  temp = det4x4(M1);
  
  for(j=1;j<=4;j=j+1){
    for(k=1;k<=4;k=k+1){
      M1[j][k] = Adj[j][k]/temp;
    }
  }
}

double pointonspline(double s, double a[4]){
  double temp;  
  temp = a[0] + a[1]*s + a[2]*s*s + a[3]*s*s*s;
  return temp;
}

double derivofspline(double s, double a[4]){
  double temp;
  temp = a[1] + 2.0*a[2]*s + 3.0*a[3]*s*s;
  return temp;
}

void cubsplinecoeffs(double s_mm, double f_mm, double s_m, double f_m, 
		     double s_p, double f_p, double s_pp, double f_pp,
		     double a[4]){
  int i,j,k;
  double A[1+4][1+4];
  double F[1+4];

  F[1] = f_m;
  F[2] = f_p;
  F[3] = (f_p - f_mm)/(s_p - s_mm);
  F[4] = (f_pp - f_m)/(s_pp - s_m);
  
  A[1][1] = 1.0;
  A[1][2] = s_m;
  A[1][3] = s_m*s_m;
  A[1][4] = s_m*s_m*s_m;
  A[2][1] = 1.0;
  A[2][2] = s_p;
  A[2][3] = s_p*s_p;
  A[2][4] = s_p*s_p*s_p;
  A[3][1] = 0.0;
  A[3][2] = 1.0;
  A[3][3] = 2.0*s_m;
  A[3][4] = 3.0*s_m*s_m;
  A[4][1] = 0.0;
  A[4][2] = 1.0;
  A[4][3] = 2.0*s_p;
  A[4][4] = 3.0*s_p*s_p;
  invert4x4(A);
  for(k=0;k<=3;k=k+1){
    a[k] = dot4(A[k+1],F);
  }
}

void quadsplinecoeffs(double s_m, double f_m, double s_p, 
		      double f_p, double s_pp, double f_pp,
		      double a[4]){
  int i,j,k;
  double A[1+3][1+3];
  double F[1+3];

  F[1] = f_m;
  F[2] = f_p;
  
  A[1][1] = 1.0;
  A[1][2] = s_m;
  A[1][3] = s_m*s_m;
  A[2][1] = 1.0;
  A[2][2] = s_p;
  A[2][3] = s_p*s_p;
  A[3][1] = 0.0;
  A[3][2] = 1.0;
  if(s_pp > s_p){
    A[3][3] = 2.0*s_p;
    F[3] = (f_pp - f_m)/(s_pp-s_m);
  }
  else{
    if(s_pp < s_m){
      A[3][3] = 2.0*s_m;
      F[3] = (f_p - f_pp)/(s_p-s_pp);
    }
    else{
      printf("\nERROR: s_pp QUADSPLINE INPUT INSIDE INTERVAL");
    }
  }
  invert3x3(A);
  for(k=0;k<=2;k=k+1){
    a[k] = dot(A[k+1],F);
  }
  a[3] = 0.0;
}

double dot4(double v1[1+4], double v2[1+4]){
  int j;
  double temp;
  
  temp = 0.0;
  for(j=1;j<=4;j=j+1){
    temp = temp + v1[j]*v2[j];
  }
  return temp;
}     

void diagonalizeNfreeD(double hmwc[1+Nfree*D][1+Nfree*D],
		       double smwc[1+Nfree*D][1+Nfree*D],
		       double w2[1+Nfree*D], int ndiag){
  int i, j, k, nonnull;
  int test;
  double temp;
  double diff, tolerance, relerror, evalue;

  double K[1+Nfree*D][1+Nfree*D];
  double x[3][1+Nfree*D];

  for(j=1;j<=Nfree*D;j=j+1){
    for(k=1;k<=Nfree*D;k=k+1){
      K[j][k] = hmwc[j][k];
    }
  }
  tolerance = .000001;

  nonnull = Nfree*D;
  if(Nfree == N){
    nonnull = N*D-6;
  }
  
  for(i=1;i<=nonnull;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      x[0][j]=randomf(-1.0,1.0);
      x[1][j] = 0.0;
      x[2][j] = 0.0;
    }
    normalizeNfreeD(x[0]);
		   
    test = 0;          
    for(k=1;test == 0;k=k+1){  
      for(j=1;j<=Nfree*D;j=j+1){ 
	x[k%3][j]=dotNfreeD(K[j],x[(k-1)%3]);                 
      }
      normalizeNfreeD(x[k%3]);  
                   
      relerror = 0.0;
      for(j=1;j<=Nfree*D;j=j+1){
	diff = x[k%3][j]-x[(k-2)%3][j];      
	relerror = relerror + fabs(diff);          
      }
      
      if((relerror < tolerance && k > 1000) || (k == ndiag)){
        if(DEBUG == 1){
	  printf("  ");
	  if(k == ndiag){
	    printf("\nitmax reached:");
	    printf("    relative error in evect %d: %f",i,relerror);
	  }
	}
	for(j=1;j<=Nfree*D;j=j+1){
	  x[(k+1)%3][j] = dotNfreeD(K[j],x[k%3]);
	}
	w2[i] = dotNfreeD(x[k%3],x[(k+1)%3]);
	for(j=1;j<=Nfree*D;j=j+1){           
	  smwc[i][j] = x[k%3][j];  
	}
	projectfrommatrix(smwc[i],K);
	test = 1;                                      
      }                      
    }
  }   
}

void diagonalize3x3(double hmwc[1+3][1+3],
		       double smwc[1+3][1+3],
		       double w2[1+3], int ndiag){
  int i, j, k;
  int test;
  double temp;
  double diff, tolerance, relerror, evalue;

  double K[1+3][1+3];
  double x[3][1+3];

  for(j=1;j<=D;j=j+1){
    for(k=1;k<=D;k=k+1){
      K[j][k] = hmwc[j][k];
    }
  }
  tolerance = .000001;
  if(DEBUG == 1){
    printf("\n  DIAGONALIZING 3x3:");
    fflush(stdout);
  }
  
  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      x[0][j]=randomf(-1.0,1.0);
      x[1][j] = 0.0;
      x[2][j] = 0.0;
    }
    normalizeD(x[0]);
		   
    test = 0;          
    for(k=1;test == 0;k=k+1){  
      for(j=1;j<=D;j=j+1){ 
	x[k%3][j]=dot(K[j],x[(k-1)%3]);                 
      }
      normalizeD(x[k%3]);  
                   
      relerror = 0.0;
      for(j=1;j<=D;j=j+1){
	diff = x[k%3][j]-x[(k-2)%3][j];      
	relerror = relerror + fabs(diff);          
      }
      
      if((relerror < tolerance && k > 100) || (k == ndiag)){
        if(DEBUG == 1){
	  printf(".");
	  if(k == ndiag){
	    printf("\n  itmax reached:");
	    printf("    relative error in evect %d: %f",i,relerror);
	  }
	}
	for(j=1;j<=D;j=j+1){
	  x[(k+1)%3][j] = dot(K[j],x[k%3]);
	}
	w2[i] = dot(x[k%3],x[(k+1)%3]);
	if(DEBUG == 1){
	  printf(" (%.2f) ",w2[i]);
        }
	for(j=1;j<=D;j=j+1){           
	  smwc[i][j] = x[k%3][j];  
	}
	projectfrommatrix3x3(smwc[i],K);
	test = 1;                                      
      }                      
    }
  }   
}

/*******************  computes vector dot product  **********************/

double dot(double v[D+1], double u[D+1]){
  int j,k;
  double dp;
  dp = 0.0;
  for(k=1;k<=D;k=k+1){
    dp = dp+v[k]*u[k];
  }
  return dp;
}

void DxDdotDx1(double matrixDxD[1+D][1+D], double vectorDx1[D+1], 
	       double productDx1[D+1]){
  int k;
  double temp[1+D];            /* makes possible to overwrite v with v   */
  for(k=1;k<=D;k=k+1){
    temp[k]=dot(matrixDxD[k],vectorDx1);
  }                            /* note v and u treated as column vectors */
  for(k=1;k<=D;k=k+1){
    productDx1[k] = temp[k];
  }
}

double dotND(double v[1+N*D], double u[1+N*D]){
  int j;
  double dp;
  dp = 0.0;
  for(j=1;j<=N*D;j=j+1){
    dp = dp + v[j]*u[j];
  }
  return dp;
}

double dotNfreeD(double v[1+Nfree*D], double u[1+Nfree*D]){
  int j;
  double dp;
  dp = 0.0;
  for(j=1;j<=Nfree*D;j=j+1){
    dp = dp + v[j]*u[j];
  }
  return dp;
}

/*********************  cross product: R1xR2=R3  ************************/

void cross(double R1[1+3], double R2[1+3], double R3[1+3]){
  R3[1]=R1[2]*R2[3]-R1[3]*R2[2];
  R3[2]=0.0-R1[1]*R2[3]+R1[3]*R2[1];
  R3[3]=R1[1]*R2[2]-R1[2]*R2[1];
}

/********  generates random number (float) in interval [a,b]  ***********/ 

double randomf(double a, double b){
  double aa;
  aa = ((double)( rand()%1001))/1000.0;
  return a+(b-a)*aa;
}

int randomint(int a, int b){
  int diff, aa;
  diff = b - a + 1;
  aa = rand()%diff;
  return (a+aa);
}

void normalizeD(double x[1+D]){
  double temp;
  int i;
  temp = sqrt(dot(x,x));
  for(i=1;i<=D;i=i+1){
    x[i] = x[i]/temp;
  }
}

void normalize(double xyz[1+N*D]){
  double numer;
  int j;
  numer = 0.0;
  for(j=1;j<=N*D;j=j+1){
    numer = numer + xyz[j]*xyz[j];
  }                                         /* this function used often */
                                            /* better not to call dotND */
  numer = sqrt(numer);
  for(j=1;j<=N*D;j=j+1){
    xyz[j] = xyz[j]/numer;
  }
}



void rottrans_evects_mwc(double xyzM[1+N][1+D], double rootM[1+N*D],
			 double rotation[1+3][1+Nfree*D],
			 double translation[1+3][1+Nfree*D]){
  int i,j,k;
  double temp[1+D];

  temp[1] = temp[2] = temp[3] = temp[0] = 0.0;
  for(i=1;i<=N;i=i+1){
    temp[0] = temp[0] + (double)Mass[i];
    for(k=1;k<=D;k=k+1){
      temp[k] = temp[k] + xyzM[i][k]*rootM[(i-1)*D+k];
    }
  }
  for(k=1;k<=D;k=k+1){
    temp[k] = temp[k]/temp[0];
  }
  printf("\n  C.O.M FOR ROT/TRANS CONSTRUCTION: (%f, %f, %f)",
	 temp[1], temp[2], temp[3]);

  for(k=0;k<=D-1;k=k+1){
    for(j=1;j<=Nfree*D;j=j+1){
      translation[k+1][j] = 0.0;
      if(j%D == k){
	translation[k+1][j] = rootM[j];
      }
    }
    normalizeNfreeD(translation[k+1]);
  }
  
  for(j=1;j<=Nfree;j=j+1){
    rotation[1][(j-1)*D+1] = 0.0;
    rotation[1][(j-1)*D+2] = xyzM[j][3];
    rotation[1][(j-1)*D+3] = -xyzM[j][2];
    
    rotation[2][(j-1)*D+1] = -xyzM[j][3];
    rotation[2][(j-1)*D+2] = 0.0;
    rotation[2][(j-1)*D+3] = xyzM[j][1];
    
    rotation[3][(j-1)*D+1] = xyzM[j][2];
    rotation[3][(j-1)*D+2] = -xyzM[j][1];
    rotation[3][(j-1)*D+3] = 0.0;
  }

  for(j=1;j<=D;j=j+1){
    gramschmidtNfreeD(rotation[1], translation[j], rotation[1]);
  }
  normalizeNfreeD(rotation[1]);
  gramschmidtNfreeD(rotation[2], rotation[1], rotation[2]);
  for(j=1;j<=D;j=j+1){
    gramschmidtNfreeD(rotation[2], translation[j], rotation[2]);
  }
  normalizeNfreeD(rotation[2]);
  gramschmidtNfreeD(rotation[3], rotation[1], rotation[3]);
  gramschmidtNfreeD(rotation[3], rotation[2], rotation[3]);
  for(j=1;j<=D;j=j+1){
    gramschmidtNfreeD(rotation[3], translation[j], rotation[3]);
  }
  normalizeNfreeD(rotation[3]);
}

void gramschmidt_rottrans(double xyzB[1+N][1+D], 
	double t[1+Nfree*D]){
	
  int k;
  double rotation[1+D][1+Nfree*D];
  double translation[1+D][1+Nfree*D];
  double rootM[1+N*D], xyzM[1+N][1+D];

  build_rootM(Mass, rootM);
  xyzcarttomwc(xyzB, xyzM, rootM);

  rottrans_evects_mwc(xyzM, rootM, rotation, translation);

  for(k=1;k<=D;k=k+1){
    gramschmidtNfreeD(t, rotation[k], t);
    gramschmidtNfreeD(t, translation[k], t);
  }
}

void gramschmidtNfreeD(double v_out[1+Nfree*D], double u_in[1+Nfree*D],
		       double v_in[1+Nfree*D]){
  double temp;
  double temp2;
  int i;

  temp = dotNfreeD(v_in, u_in);
  temp2 = dotNfreeD(u_in, u_in);
  for(i=1;i<=Nfree*D;i=i+1){
    v_out[i] = v_in[i]-temp*u_in[i]/temp2;
  }
}

void normalizeNfreeD(double vector[1+Nfree*D]){
  int j;
  double temp;
  
  temp = sqrt(dotNfreeD(vector, vector));
  for(j=1;j<=Nfree*D;j=j+1){
    vector[j] = vector[j]/temp;
  }
}

void projectfrommatrix(double vector[1+Nfree*D], 
		       double hmwc[1+Nfree*D][1+Nfree*D]){
  int i,j,k;
  double p[1+Nfree*D][1+Nfree*D];
  double K[1+Nfree*D][1+Nfree*D];
 
  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      if(i == j){
	p[i][j] = 1.0 - vector[i]*vector[j];
      }
      else{
	p[i][j] = - vector[i]*vector[j];
      }
    }
  }
  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      K[i][j] = 0.0;
      for(k=1;k<=Nfree*D;k=k+1){
	K[i][j] = K[i][j] + hmwc[i][k]*p[k][j];
      }
    }
  }
  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      hmwc[i][j] = 0.0;
      for(k=1;k<=Nfree*D;k=k+1){
	hmwc[i][j] = hmwc[i][j] + p[i][k]*K[k][j];
      }
    }
  }
}

void projectfrommatrix3x3(double vector[1+D], 
		       double hmwc[1+D][1+D]){
  int i,j,k;
  double p[1+D][1+D];
  double K[1+D][1+D];
 
  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      if(i == j){
	p[i][j] = 1.0 - vector[i]*vector[j];
      }
      else{
	p[i][j] = - vector[i]*vector[j];
      }
    }
  }
  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      K[i][j] = 0.0;
      for(k=1;k<=D;k=k+1){
	K[i][j] = K[i][j] + hmwc[i][k]*p[k][j];
      }
    }
  }
  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      hmwc[i][j] = 0.0;
      for(k=1;k<=D;k=k+1){
	hmwc[i][j] = hmwc[i][j] + p[i][k]*K[k][j];
      }
    }
  }
}

void sort_descending(int num, 
		    double orig_array[1+N*D], int new_positions[1+N*D]){
  int i,j,k;
  for(i=1;i<=num;i=i+1){
    k = 1;
    for(j=1;j<=num;j=j+1){
      if(i != j){
	if(orig_array[j] > orig_array[i]){
	  k = k+1;                      /* count elements less than ith */
	}
	else{
	  if(orig_array[j] == orig_array[i]){
	    if(j < i){
	      k = k+1;                  /* in case of tie, retain order */
	    }
	  }
	}                               /* new position of old ith is k */
      }
    }
    new_positions[i] = k;
  }
}

void swap_doubles(double *p,double *q){
  double tmp;
  tmp = *p;
  *p = *q;
  *q = tmp;
}

void swap_ints(int *p, int *q){
  int tmp;
  tmp = *p;
  *p = *q;
  *q = tmp;
}



void check_esystem_orthon(double L[1+Nfree*D][1+Nfree*D],
			  double rotation[1+3][1+Nfree*D],
			  double translation[1+3][1+Nfree*D]){
  int i, j, k, nonnull;
  double temp, tempsum;

  if(N == Nfree){
    nonnull = N*D-6;
  }
  else{
    nonnull = Nfree*D;
  }

  printf("\n\n  ORTHOGONALITY CHECK:");
  tempsum = 0.0;
  for(k=1;k<=nonnull;k=k+1){
    for(j=1;j<=nonnull;j=j+1){
      if(k == j){
	temp = fabs(1.0-dotNfreeD(L[k], L[j]));
	if(temp > 0.001){
	  printf("\n  %f FROM NORMALIZED NONNULL EVECTOR %d",
		 temp, k);
	}
	tempsum = tempsum + temp;
      }
      if(k != j){
	temp = fabs(dotNfreeD(L[k], L[j]));
	if(temp > 0.001){
	  printf("\n  %f OVERLAP IN NONNULL EVECTORS %d & %d",
		 temp, k, j);
	}
	tempsum = tempsum + temp;
      }
    }
  }
  if(N == Nfree){
    for(k=1;k<=D;k=k+1){
      for(j=1;j<=D;j=j+1){
	if(k == j){
	  temp = fabs(1.0-dotNfreeD(rotation[k], rotation[j]));
	  if(temp > 0.001){
	    printf("\n  %f UNNORMALIZED ROTATION EVECTOR %d",
		   temp, k);
	  }
	  tempsum = tempsum + temp;
	}
	if(k != j){
	  temp = fabs(dotNfreeD(rotation[k], rotation[j]));
	  if(temp > 0.001){
	  printf("\n  %f OVERLAP IN ROTATION EVECTORS %d & %d",
		 temp, k, j);
	  }
	  tempsum = tempsum + temp;
	}
	if(k == j){
	  temp = fabs(1.0-dotNfreeD(translation[k], translation[j]));
	  if(temp > 0.001){
	    printf("\n  %f UNNORMALIZED TRANSLATION EVECTOR %d",
		   temp, k);
	  }
	  tempsum = tempsum + temp;
	}
	if(k != j){
	  temp = fabs(dotNfreeD(translation[k], translation[j]));
	  if(temp > 0.001){
	    printf("\n  %f OVERLAP IN TRANSLATION EVECTORS %d & %d",
		   temp, k, j);
	  }
	  tempsum = tempsum + temp;
	}
	temp = fabs(dotNfreeD(rotation[k], translation[j]));
	if(temp > 0.001){
	  printf("\n  %f ROTATION/TRANSLATION OVERLAP EVECTORS %d & %d",
		 temp, k, j);
	}
	tempsum = tempsum + temp;
      }
    }
    for(k=1;k<=D;k=k+1){
      for(j=1;j<=nonnull;j=j+1){
      temp = fabs(dotNfreeD(rotation[k], L[j]));
      if(temp > 0.001){
	printf("\n  %f OVERLAP ROTATION/NONNULL EVECTORS %d & %d",
	       (float)temp, k, j);
      }
      tempsum = tempsum + temp;
      temp = fabs(dotNfreeD(translation[k], L[j]));
      if(temp > 0.001){
	printf("\n  %f OVERLAP TRANSLATION/NONNULL EVECTORS %d & %d",
	       (float)temp, k, j);
      }
      tempsum = tempsum + temp;
      }
    }
  }

  printf("\n    Sumij|(L.Lt)-I|ij = %f",(float)temp);
}


void SR1_update(double xyzB[1+N][1+D], double dx[1+Nfree*D],
                /* dx passed as mwc value if using mwcs */
		double g_new[1+Nfree*D], double g_old[1+Nfree*D],
		double h[1+Nfree*D][1+Nfree*D]){

  int i, j, k;
  double t[1+Nfree*D], y[1+Nfree*D];
  double r, sr1_denom, dx_size, t_size;
  double rootM[1+N*D], xyzM[1+N][1+D], Hdx[1+Nfree*D];

  build_rootM(Mass, rootM);
  xyzcarttomwc(xyzB, xyzM, rootM);
  
  r = 0.0000001;
  for(j=1;j<=Nfree*D;j=j+1){
	y[j] = g_new[j] - g_old[j];
	Hdx[j] = dotNfreeD(h[j], dx);
  }
  for(i=1;i<=Nfree*D;i=i+1){
        t[i] = y[i] - Hdx[i];
	if(DEBUG == 1){
        	printf(" %.2f", t[i]);
	}
  }
  /*
  if(Nfree == N  &&  N!=1){
    gramschmidt_rottrans(xyzB, t);
  }
  */
  sr1_denom = dotNfreeD(t, dx);
  dx_size = sqrt(dotNfreeD(dx, dx));
  t_size = sqrt(dotNfreeD(t, t));

  if( fabs(sr1_denom) < r*dx_size*t_size ){

    printf("\n\n  SKIP UPDATE (SEE p.202-204 NOCEDAL & WRIGHT)");
    printf("\n    |(y-Hdx).dx|/(|dx||y-Hdx|):r :: %e:%e",
        			sr1_denom/(dx_size*t_size), r);
    printf("\n    |(y-Hdx).dx|:r :: %e:%e", 
        			fabs(sr1_denom), r);

  }else{
    printf("\n  SR1 HESSIAN UPDATE");
    for(i=1;i<=Nfree*D;i=i+1){
      for(j=1;j<=Nfree*D;j=j+1){
  	h[i][j] = h[i][j] + t[i]*t[j]/sr1_denom;
      }
    }
  }
}



void BFGS_update(double xyzB[1+N][1+D], double dx[1+Nfree*D],
		double g_new[1+Nfree*D], double g_old[1+Nfree*D],
		double h[1+Nfree*D][1+Nfree*D]){

	int i, j, k;
	double Hdx[1+Nfree*D], y[1+Nfree*D], dxy;
	double r[1+Nfree*D], theta, dxHdx, dxr;
	double rotation[1+D][1+Nfree*D];
	double translation[1+D][1+Nfree*D];

  printf("\n  DAMPED BFGS HESSIAN UPDATE p.541 NOCEDAL & WRIGHT");
  
  printf("\n    GRAM-SCHMIDT NULL SPACE COMPONENTS FROM SECANT");

  for(j=1;j<=Nfree*D;j=j+1){
	y[j] = g_new[j] - g_old[j];
  }

  if(Nfree == N &&  N!=1){
    gramschmidt_rottrans(xyzB, y);
    gramschmidt_rottrans(xyzB, dx);
  }

  printf("\n  H.dx:");
  for(i=1;i<=Nfree*D;i=i+1){
    Hdx[i] = dotNfreeD(h[i], dx);
    printf(" %.2f",Hdx[i]);
  }
  dxHdx = dotNfreeD(dx, Hdx);
  dxy = dotNfreeD(dx, y);

  if( dxy > 0.2*dxHdx ){
	theta = 1.0;
  }else{
	theta = 0.8*dxHdx/(dxHdx-dxy);
  }

  for(i=1;i<=Nfree*D;i=i+1){
	r[i] = theta*y[i]+(1.0-theta)*Hdx[i];
  }

  dxr = dotNfreeD(r, dx);

  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      h[i][j] = h[i][j] - Hdx[i]*Hdx[j]/dxHdx + r[i]*r[j]/dxr;
    }
  }
}

void dBFGS_update(double xyzB[1+N][1+D], double dx[1+Nfree*D],
		double dg[1+Nfree*D], double h[1+Nfree*D][1+Nfree*D],
		double h_new[1+Nfree*D][1+Nfree*D]){

	int i, j, k;
	double Hdx[1+Nfree*D], dxdg;
	double r[1+Nfree*D], theta, dxHdx, dxr;
	double rotation[1+D][1+Nfree*D];
	double translation[1+D][1+Nfree*D];

  printf("\n  DAMPED BFGS HESSIAN UPDATE p.541 NOCEDAL & WRIGHT");
  
  printf("\n    GRAM-SCHMIDT NULL SPACE COMPONENTS FROM SECANT");


  if(Nfree == N &&  N!=1){
    gramschmidt_rottrans(xyzB, dg);
    gramschmidt_rottrans(xyzB, dx);
  }

  if(DEBUG == 1){ printf("\n  H.dx:"); }
  for(i=1;i<=Nfree*D;i=i+1){
    Hdx[i] = dotNfreeD(h[i], dx);
    if(DEBUG == 1){ printf(" %.2f",Hdx[i]); }
  }
  dxHdx = dotNfreeD(dx, Hdx);
  dxdg = dotNfreeD(dx, dg);

  if( dxdg > 0.2*dxHdx ){
	theta = 1.0;
  }else{
	theta = 0.8*dxHdx/(dxHdx-dxdg);
  }

  for(i=1;i<=Nfree*D;i=i+1){
	r[i] = theta*dg[i]+(1.0-theta)*Hdx[i];
  }

  dxr = dotNfreeD(r, dx);

  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      h_new[i][j] = h[i][j] - Hdx[i]*Hdx[j]/dxHdx + r[i]*r[j]/dxr;
    }
  }
}



void DxDdotDxD(double M1[1+D][1+D], 
	double M2[1+D][1+D], double M3[1+D][1+D]){

  int i, j, k;
  double temp, M4[1+D][1+D];

  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      M4[i][j] = 0.0;
      for(k=1;k<=D;k=k+1){

      	M4[i][j] = M4[i][j] + M1[i][k]*M2[k][j];

      }
    }
  }
  for(i=1;i<=D;i=i+1){
    for(j=1;j<=D;j=j+1){
      M3[i][j] = M4[i][j];
    }
  }
}

void addNfreeD(double x[1+Nfree*D], 
	double y[1+Nfree*D], double z[1+Nfree*D]){
  int i;
 
  for(i=1;i<=Nfree*D;i=i+1){
    z[i] = x[i] + y[i];
  }
}
 
void subtractNfreeD(double x[1+Nfree*D], 
	double y[1+Nfree*D], double z[1+Nfree*D]){
  int i;
 
  for(i=1;i<=Nfree*D;i=i+1){
    z[i] = x[i] - y[i];
  }
}

void cloneNfreeD(double x[1+Nfree*D], double y[1+Nfree*D]){
 
  int i;
 
  for(i=1;i<=Nfree*D;i=i+1){
    y[i] = x[i];
  }
}

void cloneND(double x[1+N*D], double y[1+N*D]){
 
  int i;
 
  for(i=1;i<=N*D;i=i+1){
    y[i] = x[i];
  }
}

void cloneNfreeDxNfreeD(double x[1+Nfree*D][1+Nfree*D],
	double y[1+Nfree*D][1+Nfree*D]){
  int i, j;
  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      y[i][j] = x[i][j];
    }
  }
}

