
/************************************************************************/
/****************  Q-Chem Program Interface Routines  *******************/
/************************************************************************/
  
int read_grad_job_count(void){
	FILE *gradjobs;
	int k;
	gradjobs = fopen("gradjobcount.txt","r");
	if(gradjobs == NULL){
		printf("\n  GRADIENT JOB COUNTS DOESN'T EXIST");
		fclose(gradjobs);
		printf("\n  CREATING FILE AND INITIALIZING COUNT TO 0");
		gradjobs = fopen("gradjobcount.txt","w");
		fprintf(gradjobs, "%d ",0);
		k = 0;
	}else{
		fscanf(gradjobs,"%d",&k);
	}
	fclose(gradjobs);
	return k;
}

int count_grad_jobs(void){
	FILE *gradjobs;
	int k;
	gradjobs = fopen("gradjobcount.txt","r");
	if(gradjobs == NULL){
		printf("\n  GRADIENT JOB COUNTS DOESN'T EXIST");
		fclose(gradjobs);
		printf("\n  CREATING FILE AND INITIALIZING COUNT TO 0");
		gradjobs = fopen("gradjobcount.txt","w");
		fprintf(gradjobs, "%d ",1);
	}else{
		fscanf(gradjobs,"%d",&k);
		if(DEBUG >= 1){
			printf("  %d", k+1);
		}
		fclose(gradjobs);
		gradjobs = fopen("gradjobcount.txt","w");
		fprintf(gradjobs, "%d ",k+1);
	}
	fclose(gradjobs);
	return k+1;
}	


double scf_grad_cart(double xyzB[1+N][1+D],
        double g[1+Nfree*D]){

  double V;
  int i,j,k;
  time_t start_time, end_time;

  time(&start_time);
  V = VdVatxyzB(xyzB, g);
  time(&end_time);
  fflush(stdout);

  /************************************************************/
  /*
  printf("\n\n  V/(H) = %f",V);
  printf("  grad calculation took %d seconds",
	 end_time-start_time);
  printgradient(g);
  */
  fflush(stdout);
  return V;
}

double scf_grad_mwc(double xyzB[1+N][1+D],
        		double gmwc[1+Nfree*D]){

  double g[1+Nfree*D], V;
  double xyzM[1+N][1+D], rootM[1+N*D];
  int i,j,k, jobs;
  time_t start_time, end_time;

  build_rootM(Mass, rootM);	

  time(&start_time);
  V = VdVatxyzB(xyzB, g);
  time(&end_time);
  gcarttomwc(g,gmwc,rootM);

  /*************************************************************/
  /*
  printf("\n\n  V/(H) = %f", V);
  printf(" grad calculation took %d seconds",
	 end_time-start_time);
  printmwcgradient(gmwc);
  */
  fflush(stdout);
  return V;
}

/**************************************************************/
/*************  ELECTRONIC STRUCTURE CALCULATION  *************/
/**************************************************************/


double VdVatxyzB(double xyzB[1+N][1+D], double g[1+Nfree*D]){

  FILE *outfile, *infile, *qcoptions;
  int num,j,k,c,i;
  double G[1+N*D], xyzA[1+N][1+D], V, temp;
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};

  bohrtoangstrom(xyzB,xyzA);
  system("rm -f g.in");
  outfile = fopen("g.in","w");
  fprintf(outfile,"%d %d\n", Charge, Spin);
  for(j=1;j<=N;j=j+1){
    fprintf(outfile,"%s %f %f %f\n",
	    symbol[Number[j]],
	    xyzA[j][1],xyzA[j][2],xyzA[j][3]);
  }
  fprintf(outfile, "\n");
  fclose(outfile);
  system("cat remdV g.in basis endoptions> i");
  system("cp i g.in");
  // system("nohup g16 <g.in> g.out");
  system("nohup g16 g.in g.out");

  scangradient(g);
  V = scanenergy();
  return V;
}

double scanenergy(void){

  int i;
  double temp;
  FILE *gfile;
  FILE *energy;
  char indicator[100] = {"SCF Done: "};
  char currentline[100] = {"Something silly"};
  char tokens[] = "=A";
  char *p;
  gfile = fopen("g.out", "r");

  while(strstr(currentline,indicator) == NULL){
    fgets(currentline,100,gfile);
  }
  printf("\n %s\n", currentline);
  energy = fopen("energy","w");
  p = strtok(currentline, tokens);
  p = strtok(NULL, tokens);
  fprintf(energy, "%s", p);
  fclose(energy);
  fclose(gfile);
  /* SCF Done:  E(RB+HF-LYP) =  -76.4197366351     A.U. after    9 cycles */
  energy = fopen("energy","r");
  fscanf(energy, "%lf", &temp);
  fclose(energy);
  return temp;
}

void scangradient(double g[1+Nfree*D]){

  int i, j, k;
  double temp;
  FILE *gfile, *grad7;
  char indicator[100] = {"Forces (Hartrees/Bohr)"};
  char currentline[100] = {"Something silly"};
  gfile = fopen("g.out", "r");
  while(strstr(currentline,indicator) == NULL){
    fgets(currentline, 100, gfile);
  }
  fgets(currentline, 100, gfile);
  fgets(currentline, 100, gfile);
  grad7 = fopen("grad.7","w");
  for(i=1;i<=Nfree;i=i+1){
    fgets(currentline, 100, gfile);
    fprintf(grad7, "%s ", currentline);
  }
  fclose(grad7);
  fclose(gfile);

  gfile = fopen("grad.7", "r");
  for(i=1;i<=Nfree;i=i+1){
    fscanf(gfile, "%d %d", &j, &k);
    fscanf(gfile, "%lf ", &temp);
    g[(i-1)*D+1] = -temp;
    fscanf(gfile, "%lf ", &temp);
    g[(i-1)*D+2] = -temp;
    fscanf(gfile, "%lf ", &temp);
    g[(i-1)*D+3] = -temp;
  }
  fclose(gfile);
}

double VdVd2VatxyzB(double xyzB[1+N][1+D], 
	double g[1+Nfree*D], double h[1+Nfree*D][1+Nfree*D]){

  FILE *outfile, *infile;
  FILE *optsfile, *qcoptions;
  int num,j,k,c,i;
  double H[1+N*D][1+N*D], G[1+N*D];
  double xyzA[1+N][1+D],V, temp;
  char symbol[103][3]={"H?",
		       "H","He","Li","Be","B","C","N","O","F","Ne",
		       "Na","Mg","Al","Si","P","S","Cl","Ar",
		       "K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co",
		       "Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
		       "Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh",
		       "Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe",
		       "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu",
		       "Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf",
		       "Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl",
		       "Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",
		       "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es",
		       "Fm","Md","No"};
 
  bohrtoangstrom(xyzB,xyzA); 
  system("rm -f g.in");
  outfile = fopen("g.in","w");
  fprintf(outfile,"%d %d\n", Charge, Spin);
  for(j=1;j<=N;j=j+1){
    fprintf(outfile,"%s %f %f %f\n",
            symbol[Number[j]],
            xyzA[j][1],xyzA[j][2],xyzA[j][3]);
  }
  fprintf(outfile, "\n");
  fclose(outfile);
  system("cat remd2V g.in basis endoptions> i");
  system("cp i g.in");
  system("cp g.out grad");
  // system("nohup g16 <g.in> g.out");
  system("nohup g16 g.in g.out");
  system("cp g.out hessian");
  system("sed -e 's/D/E/g' fort.7 > i");
  system("cp i fort.7");

  outfile = fopen("fort.7","r");
  if(outfile == NULL){
    printf("\n ERROR OPENING QCHEM OUTPUT FILE: fort.7 ");
  }
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      fscanf(outfile, "%lf ", &temp);
      G[(i-1)*D+j] = temp;
    }
  }
  optsfile = fopen("fake.7","w");
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      fprintf(optsfile, "%lf ", G[(i-1)*D+j]);
    }
    fprintf(optsfile, "\n");
  }
  k = 0;
  for(i=1;i<=(N*D);i=i+1){
    for(j=1;j<=i;j=j+1){
      k = k + 1;
      fscanf(outfile, "%lf ", &temp);
      if(k%3 == 1){
	fprintf(optsfile, "\n");
      }
      H[i][j] = temp;
      fprintf(optsfile, "%lf ", H[i][j]);
    }
  }
  for(i=1;i<=(Nfree*D);i=i+1){
    g[i] = G[i];
    for(j=1;j<=i;j=j+1){
      h[i][j] = H[i][j];
      h[j][i] = h[i][j];
    }
  }
  fclose(outfile);
  fclose(optsfile);
  V = scanenergy();
  return V;
}

double scf_freq_diag_analysis(double xyzB[1+N][1+D],
			      double gmwc[1+Nfree*D],
			      double w2[1+Nfree*D],
			      double smwc[1+Nfree*D][1+Nfree*D], 
			      double hmwc[1+Nfree*D][1+Nfree*D], 
			      double sdotg[1+Nfree*D]){
  double g[1+Nfree*D];
  double h[1+Nfree*D][1+Nfree*D], V;
  double translation[1+D][1+Nfree*D];
  double rotation[1+D][1+Nfree*D];
  double xyzM[1+N][1+D], rootM[1+N*D];
  int i,j,k;
  time_t start_time, end_time;
  
  build_rootM(Mass, rootM);

  time(&start_time);
  V = VdVd2VatxyzB(xyzB, g, h);
  time(&end_time);
  printf("\n  electronic structure calculation took %d seconds", 
	 end_time-start_time);
  fflush(stdout);


  hcarttomwc(h,hmwc,rootM);
  gcarttomwc(g,gmwc,rootM);
  xyzcarttomwc(xyzB, xyzM, rootM);

  /***************************************************************/
  /*******  GENERATE NULL SPACE EIGENVECTORS & PROJECT ***********/
  /***************************************************************/
  
  if(Nfree == N){
    rottrans_evects_mwc(xyzM, rootM, rotation, translation);
    for(k=1;k<=D;k=k+1){
      printf("\n  projecting translation: g.t = %f",
	     dotNfreeD(gmwc,translation[k]));
      projectfrommatrix(translation[k],hmwc);
    }
    for(k=1;k<=D;k=k+1){
      printf("\n  projecting rotation: g.r = %f",
	     dotNfreeD(gmwc,rotation[k]));
      projectfrommatrix(rotation[k],hmwc);
    }
  }
  fflush(stdout);
 
  /************************************************************/
  /***************  DIAGONALIZE HESSIAN  **********************/
  /************************************************************/
  
  time(&start_time);
  diagonalizeNfreeD(hmwc, smwc, w2, 10000);
  time(&end_time);

  /************************************************************/
  /***************  REPLACE ROT/TRANS EIGENVECTORS  ***********/
  /************************************************************/

  if(N == Nfree){
    for(j=N*D-5;j<=N*D;j=j+1){
      w2[j] = 0.0;
    }
    for(j=1;j<=N*D;j=j+1){
      smwc[N*D-5][j] = rotation[1][j];
      smwc[N*D-4][j] = rotation[2][j];
      smwc[N*D-3][j] = rotation[3][j];
      smwc[N*D-2][j] = translation[1][j];
      smwc[N*D-1][j] = translation[2][j];
      smwc[N*D-0][j] = translation[3][j];
    }
  }
  check_esystem_orthon(smwc, rotation, translation);
  
  /************************************************************/
  /*************  NORMAL MODE ANALYSIS COMPLETE  **************/
  /************************************************************/

  printf("\n\n  V/(H) = %f",V);
  printmwcgradient(gmwc);
  printnormalmodes(w2, smwc); 
  for(j=1;j<=Nfree*D;j=j+1){
    sdotg[j] = dotNfreeD(smwc[j], gmwc);
  }
  printsmwcdotgmwc(sdotg);
  printf("\n  normal mode analysis took %d seconds", 
	 end_time-start_time);
  fflush(stdout);
  return V;
} 

void esc_death_counter(void){

 int k;
 FILE *gradjobs;
 
 gradjobs = fopen("deadjobcount.txt","r");
	if(gradjobs == NULL){
		printf("\n  DEAD JOB COUNTS DOESN'T EXIST");
		fclose(gradjobs);
		printf("\n  CREATING FILE AND INITIALIZING COUNT TO 0");
		gradjobs = fopen("deadjobcount.txt","w");
		fprintf(gradjobs, "%d ",1);
	}else{
		fscanf(gradjobs,"%d",&k);
		if(DEBUG >= 1){
			printf("  %d", k+1);
		}
		fclose(gradjobs);
		gradjobs = fopen("deadjobcount.txt","w");
		fprintf(gradjobs, "%d ",k+1);
	}
	fclose(gradjobs);
}	

int read_dead_job_count(void){

	FILE *gradjobs;
	int k;
	gradjobs = fopen("deadjobcount.txt","r");
	if(gradjobs == NULL){
		printf("\n  DEAD JOB COUNTS DOESN'T EXIST");
		fclose(gradjobs);
		printf("\n  CREATING FILE AND INITIALIZING COUNT TO 0");
		gradjobs = fopen("deadjobcount.txt","w");
		fprintf(gradjobs, "%d ",0);
		k = 0;
	}else{
		fscanf(gradjobs,"%d",&k);
	}
	fclose(gradjobs);
	return k;
}

void initial_hessian_ts(double xyzB[1+N][1+D], 
	double pathtangent[1+Nfree*D], double w2au,
	double hmwc[1+Nfree*D][1+Nfree*D]){

  int i, j, k;
  double translation[1+D][1+Nfree*D];
  double rotation[1+D][1+Nfree*D];
  double rootM[1+N*D], xyzM[1+N][1+D];

  build_rootM(Mass, rootM);
  xyzcarttomwc(xyzB, xyzM, rootM);

  if(DEBUG == 1){
	printsmwcdotgmwc(pathtangent);
	printstate(xyzM);
  }

  if(Nfree == N  &&  N != 1 ){
    rottrans_evects_mwc(xyzM, rootM, rotation, translation);
    for(k=1;k<=D;k=k+1){
      gramschmidtNfreeD(pathtangent, translation[k], pathtangent);
    }
    for(k=1;k<=D;k=k+1){
      gramschmidtNfreeD(pathtangent, rotation[k], pathtangent);
    }
  }
  normalizeNfreeD(pathtangent);

  if(DEBUG == 1){
	printsmwcdotgmwc(translation[1]);
	printsmwcdotgmwc(translation[2]);
	printsmwcdotgmwc(translation[3]);
	
	printsmwcdotgmwc(rotation[1]);
	printsmwcdotgmwc(rotation[2]);
	printsmwcdotgmwc(rotation[3]);
  }
 
  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      hmwc[i][j] = 0.0;
    }
    hmwc[i][i] = 1.5/rootM[i]+randomf(0.0, 0.1);	
  }
  if(DEBUG == 1){
    printhessian(hmwc);
    printsmwcdotgmwc(pathtangent);
  }
 
  projectfrommatrix(pathtangent, hmwc);

  for(i=1;i<=Nfree*D;i=i+1){
    for(j=1;j<=Nfree*D;j=j+1){
      hmwc[i][j] = hmwc[i][j] + w2au*pathtangent[i]*pathtangent[j];
    }
  }
  fflush(stdout);
}

void initial_hessian_min(double xyzB[1+N][1+D], 
	double h[1+Nfree*D][1+Nfree*D]){

  int i, j, k;
  double translation[1+D][1+Nfree*D];
  double rotation[1+D][1+Nfree*D];
  double rootM[1+N*D], xyzM[1+N][1+D];
  double hmwc[1+Nfree*D][1+Nfree*D];
 
  for(i=1;i<=Nfree*D;i=i+1){
 	for(j=1;j<=Nfree*D;j=j+1){
		h[i][j] = 0.0;
	}
	h[i][i] = 1.5+randomf(0.0, 0.1);	
  }
  if(Nfree == N  &&  N!=1 ){
    build_rootM(Mass, rootM);
    xyzcarttomwc(xyzB, xyzM, rootM);
    hcarttomwc(h, hmwc, rootM);
    if(Nfree == N && N!=1){
      rottrans_evects_mwc(xyzM, rootM, rotation, translation);
      for(k=1;k<=D;k=k+1){
        projectfrommatrix(translation[k], hmwc);
      }
      for(k=1;k<=D;k=k+1){
        projectfrommatrix(rotation[k], hmwc);
      }
    }
    hmwctocart(hmwc, h, rootM);
    fflush(stdout);
  }

}

void normal_mode_carts(double xyzB[1+N][1+D],
			double V,
			double g[1+Nfree*D], 
			double h[1+Nfree*D][1+Nfree*D],
			double K[1+Nfree*D],
			double S[1+Nfree*D][1+Nfree*D],
			double Sdotg[1+Nfree*D]){

  double translation[1+D][1+Nfree*D];
  double rotation[1+D][1+Nfree*D];
  double xyzM[1+N][1+D], rootM[1+N*D];
  double hmwc[1+Nfree*D][1+Nfree*D];
  int i,j,k;
  time_t start_time, end_time;

  if(N == Nfree  &&  N != 1){
  build_rootM(Mass, rootM);
  xyzcarttomwc(xyzB, xyzM, rootM);
  hcarttomwc(h, hmwc, rootM);
  if(Nfree == N && N!=1){
    gramschmidt_rottrans(xyzB, g);
    rottrans_evects_mwc(xyzM, rootM, rotation, translation);
    for(k=1;k<=D;k=k+1){
      projectfrommatrix(translation[k], hmwc);
    }
    for(k=1;k<=D;k=k+1){
      projectfrommatrix(rotation[k], hmwc);
    }
  }
  hmwctocart(hmwc, h, rootM);
  fflush(stdout);
  }

  time(&start_time);
  diagonalizeNfreeD(h, S, K, 20000);
  time(&end_time);


  if(N == Nfree){
    for(j=N*D-5;j<=N*D;j=j+1){
      K[j] = 0.0;
    }
    for(j=1;j<=N*D;j=j+1){
      S[N*D-5][j] = rotation[1][j];
      S[N*D-4][j] = rotation[2][j];
      S[N*D-3][j] = rotation[3][j];
      S[N*D-2][j] = translation[1][j];
      S[N*D-1][j] = translation[2][j];
      S[N*D-0][j] = translation[3][j];
    }
  }
  check_esystem_orthon(S, rotation, translation);
  
  /************************************************************/
  /*************  NORMAL MODE ANALYSIS COMPLETE  **************/
  /************************************************************/

  printf("\n\n  V/(H) = %f",V);
  printgradient(g);
  printeigensystem_carts(K, S); 
  for(j=1;j<=Nfree*D;j=j+1){
    Sdotg[j] = dotNfreeD(S[j], g);
  }
  printSdotG_carts(Sdotg);

  printf("\n  normal mode analysis took %d seconds", 
	 end_time-start_time);
  fflush(stdout);
}

void normal_mode_analysis(double xyzB[1+N][1+D],
			  double w2[1+Nfree*D],
			  double smwc[1+Nfree*D][1+Nfree*D], 
			  double hmwc[1+Nfree*D][1+Nfree*D]){
 
  double translation[1+D][1+Nfree*D];
  double rotation[1+D][1+Nfree*D];
  double xyzM[1+N][1+D], rootM[1+N*D];
  int i,j,k;
  
  build_rootM(Mass, rootM);

  xyzcarttomwc(xyzB, xyzM, rootM);

  /***************************************************************/
  /*******  GENERATE NULL SPACE EIGENVECTORS & PROJECT ***********/
  /***************************************************************/
  
  if(Nfree == N && N!=1){
    printf("\n  PROJECTING NULL DEGREES OF FREEDOM");
    rottrans_evects_mwc(xyzM, rootM, rotation, translation);
    for(k=1;k<=D;k=k+1){
      projectfrommatrix(translation[k],hmwc);
    }
    for(k=1;k<=D;k=k+1){
      projectfrommatrix(rotation[k],hmwc);
    }
  }
  fflush(stdout);
 
  /************************************************************/
  /***************  DIAGONALIZE HESSIAN  **********************/
  /************************************************************/
  
  diagonalizeNfreeD(hmwc, smwc, w2, 20000);

  /************************************************************/
  /***************  REPLACE ROT/TRANS EIGENVECTORS  ***********/
  /************************************************************/

  if(N == Nfree){
    for(j=N*D-5;j<=N*D;j=j+1){
      w2[j] = 0.0;
    }
    for(j=1;j<=N*D;j=j+1){
      smwc[N*D-5][j] = rotation[1][j];
      smwc[N*D-4][j] = rotation[2][j];
      smwc[N*D-3][j] = rotation[3][j];
      smwc[N*D-2][j] = translation[1][j];
      smwc[N*D-1][j] = translation[2][j];
      smwc[N*D-0][j] = translation[3][j];
    }
  }
  check_esystem_orthon(smwc, rotation, translation);
  
  /************************************************************/
  /*************  NORMAL MODE ANALYSIS COMPLETE  **************/
  /************************************************************/

  fflush(stdout);
} 

