void bohrtoangstrom(double [1+N][1+D],double [1+N][1+D]);
void angstromtobohr(double [1+N][1+D], double [1+N][1+D]);
// int round(double);
void check_esystem_orthon(double [1+Nfree*D][1+Nfree*D], 
			  double [1+3][1+Nfree*D], double [1+3][1+Nfree*D]);
double det2x2(double [1+2][1+2]);
double det3x3(double [1+3][1+3]);
double det4x4(double [1+4][1+4]);
void adjoint3x3(double [1+3][1+3], double [1+3][1+3]);
void adjoint4x4(double [1+4][1+4], double [1+4][1+4]);
void invert3x3(double [1+3][1+3]);
void invert4x4(double [1+4][1+4]);
double pointonspline(double, double [4]);
double derivofspline(double, double [4]);
void cubsplinecoeffs(double, double, double, double, 
		     double, double, double, double,
		     double [4]);
void quadsplinecoeffs(double, double, double, double,
		      double, double, double [4]);
double dot4(double [1+4], double [1+4]);
void diagonalizeNfreeD(double [1+D*Nfree][1+D*Nfree],
		       double [1+D*Nfree][1+D*Nfree],
		       double [1+D*Nfree],int);
void diagonalize3x3(double [1+D][1+D], 
			double [1+D][1+D], double [1+D], int);
void projectfrommatrix3x3(double vector[1+D], 
		       double hmwc[1+D][1+D]);

double dot(double [D+1], double [D+1]);
void DxDdotDx1(double [1+D][1+D], double [D+1], 
	       double [D+1]);
double dotND(double [N*D+1], double [N*D+1]);
double dotNfreeD(double [1+Nfree*D],double [1+Nfree*D]);
void normalizeNfreeD(double [1+Nfree*D]);
void gramschmidtNfreeD(double [1+Nfree*D], double [1+Nfree*D],
		       double [1+Nfree*D]);
void projectfrommatrix(double [1+Nfree*D], 
		       double [1+Nfree*D][1+Nfree*D]);
double randomf(double, double);
int randomint(int, int);
void normalize(double [1+N*D]);
void normalizeD(double [1+D]);
void cross(double [1+3], double [1+3], double [1+3]);

void matrixtopower(double [1+N*D][1+N*D], int , double ,
		   double [1+N*D][1+N*D]);
void DxDdotDxD(double [1+D][1+D], double [1+D][1+D], 
		   double [1+D][1+D]);
void diagonalize(int, int, double [1+D*N][1+D*N],
		 double [1+D*N][1+D*N],double [1+D*N],int);
void sort_descending(int, double [1+N*D], int [1+N*D]);
void swap_doubles(double *, double *);
void swap_ints(int *,int *);
void rottrans_evects_mwc(double [1+N][1+D], double [1+N*D],
			 double [1+3][1+Nfree*D],
			 double [1+3][1+Nfree*D]);
void gramschmidt_rottrans(double [1+N][1+D], 
	double [1+Nfree*D]);
void SR1_update(double [1+N][1+D], double [1+Nfree*D],
		double [1+Nfree*D], double [1+Nfree*D],
		double [1+Nfree*D][1+Nfree*D]);
void BFGS_update(double [1+N][1+D], double [1+Nfree*D],
		double [1+Nfree*D], double [1+Nfree*D],
		double [1+Nfree*D][1+Nfree*D]);
void dBFGS_update(double [1+N][1+D], double [1+Nfree*D],
		double [1+Nfree*D], double [1+Nfree*D][1+Nfree*D],
                double [1+Nfree*D][1+Nfree*D]);
void cloneNfreeDxNfreeD(double [1+Nfree*D][1+Nfree*D],
	double [1+Nfree*D][1+Nfree*D]);
void cloneNfreeD(double [1+Nfree*D], double [1+Nfree*D]);
void cloneND(double [1+N*D], double [1+N*D]);
void addNfreeD(double [1+Nfree*D], 
	double [1+Nfree*D], double [1+Nfree*D]);
void subtractNfreeD(double [1+Nfree*D], 
	double [1+Nfree*D], double [1+Nfree*D]);
 
