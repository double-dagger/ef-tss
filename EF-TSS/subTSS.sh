#!/bin/bash

FNAME=$1
FNAME2=${FNAME%.*}
echo $FNAME2
CORES=$2
JOBPATH=$3

echo "Submitting "$FNAME2" from "$PWD

sbatch --export=JOBPATH=$JOBPATH,FILENAME=$FNAME2 --job-name=$FNAME2 --ntasks=$CORES --nodes=1 /home/craigv/bTSS/new-code/code/runjob.sh

