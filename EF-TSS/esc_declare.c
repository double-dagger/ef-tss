
double scf_grad_cart(double [1+N][1+D],
		     double [1+Nfree*D]);

double scf_grad_mwc(double [1+N][1+D], 
		    double [1+Nfree*D]);

double VdVatxyzB(double [1+N][1+D], double [1+Nfree*D]);
void scangradient(double g[1+Nfree*D]);
double scanenergy(void);

void esc_death_counter(void);
  
int read_dead_job_count(void);

double VdVd2VatxyzB(double [1+N][1+D], 
		double [1+Nfree*D], double [1+Nfree*D][1+Nfree*D]);

double scf_freq_diag_analysis(double [1+N][1+D],
			      double [1+Nfree*D],
			      double [1+Nfree*D],
			      double [1+Nfree*D][1+Nfree*D], 
			      double [1+Nfree*D][1+Nfree*D], 
			      double [1+Nfree*D]);
void normal_mode_carts(double [1+N][1+D], double,
			double [1+Nfree*D], 
			double [1+Nfree*D][1+Nfree*D],
			double [1+Nfree*D],
			double [1+Nfree*D][1+Nfree*D],
			double [1+Nfree*D]);
void initial_hessian_min(double [1+N][1+D], 
	double [1+Nfree*D][1+Nfree*D]);
void initial_hessian_ts(double [1+N][1+D], 
	double [1+Nfree*D], double,
	double [1+Nfree*D][1+Nfree*D]);
void normal_mode_analysis(double [1+N][1+D],
			      double [1+Nfree*D],
			      double [1+Nfree*D][1+Nfree*D], 
			      double [1+Nfree*D][1+Nfree*D]);

