void build_rootM(int Mass[1+N], double rootM[1+N*D]){
  
  int i,k;
  for(i=1;i<=N;i=i+1){
    for(k=1;k<=D;k=k+1){
      rootM[(i-1)*D+k] = sqrt((double)Mass[i]);
    }
  }
  if(DEBUG == 1){
    printf("\n  build_rootM:");
    for(i=1;i<=N*D;i=i+1){
      printf(" %.2f",rootM[i]);
    }
  }
}

void xyzmwctocart(double xyzmwc[1+N][1+D], double xyzcart[1+N][1+D],
		  double rootM[1+N*D]){
  int i,j,k;
  for(j=1;j<=N;j=j+1){
    for(k=1;k<=D;k=k+1){
      xyzcart[j][k] = xyzmwc[j][k]/rootM[(j-1)*D+k];
    }
  }
}

void xyzcarttomwc(double xyzcart[1+N][1+D], double xyzmwc[1+N][1+D],
		  double rootM[1+N*D]){
  int i,j,k;
  
  for(j=1;j<=N;j=j+1){
    for(k=1;k<=D;k=k+1){
      xyzmwc[j][k] = xyzcart[j][k]*rootM[(j-1)*D+k];
    }
  }
  /*
  if(DEBUG == 1){

    printf("\n  xyzcarttomwc:");
    for(i=1;i<=N*D;i=i+1){
      printf(" %.2f", rootM[i]);
    }
    printstate(Number, xyzcart);
    printstate(Number, xyzmwc);
    }*/
}

void Gmwctocart(double Gmwc[1+N*D], double Gcart[1+N*D], 
		double rootM[1+N*D]){
  int i,j,k;
  for(j=1;j<=N*D;j=j+1){
    Gcart[j] = Gmwc[j]*rootM[j];
  }
}

void NDx1mwctocart(double dxmwc[1+N*D], double drcart[1+N*D], 
		double rootM[1+N*D]){

  int i,j,k;
  for(j=1;j<=N*D;j=j+1){
    drcart[j] = dxmwc[j]/rootM[j];
  }
}

void Hmwctocart(double Hmwc[1+N*D][1+N*D],
		double Hcart[1+N*D][1+N*D], double rootM[1+N*D]){
  int i,j,k;
  
  for(j=1;j<=N*D;j=j+1){
    for(k=1;k<=N*D;k=k+1){
      Hcart[j][k] = Hmwc[j][k]*(rootM[j]*rootM[k]);
    }
  }
}

void NDx1carttomwc(double drcart[1+N*D], double dxmwc[1+N*D], 
		double rootM[1+N*D]){

  int i,j,k;
  for(j=1;j<=N*D;j=j+1){
    dxmwc[j] = drcart[j]*rootM[j];
  }
}

void Gcarttomwc(double Gcart[1+N*D], double Gmwc[1+N*D], 
		double rootM[1+N*D]){
  int i,j,k;
  for(j=1;j<=N*D;j=j+1){
    Gmwc[j] = Gcart[j]/rootM[j];
  }
}

void Hcarttomwc(double Hcart[1+N*D][1+N*D],
		double Hmwc[1+N*D][1+N*D], double rootM[1+N*D]){
 
  int i,j,k;
  
  for(j=1;j<=N*D;j=j+1){
    for(k=1;k<=N*D;k=k+1){
      Hmwc[j][k] = Hcart[j][k]/(rootM[j]*rootM[k]);
    }
  }
}

void gmwctocart(double Gmwc[1+Nfree*D], double Gcart[1+Nfree*D], 
		double rootM[1+N*D]){
  int i,j,k;
  for(j=1;j<=Nfree*D;j=j+1){
    Gcart[j] = Gmwc[j]*rootM[j];
  }
}

void NfreeDx1mwctocart(double dxmwc[1+Nfree*D], double drcart[1+Nfree*D], 
		double rootM[1+N*D]){

  int i,j,k;
  for(j=1;j<=Nfree*D;j=j+1){
    drcart[j] = dxmwc[j]/rootM[j];
  }
}

void hmwctocart(double Hmwc[1+Nfree*D][1+Nfree*D],
		double Hcart[1+Nfree*D][1+Nfree*D], double rootM[1+N*D]){
  int i,j,k;
  
  for(j=1;j<=Nfree*D;j=j+1){
    for(k=1;k<=Nfree*D;k=k+1){
      Hcart[j][k] = Hmwc[j][k]*(rootM[j]*rootM[k]);
    }
  }
}

void NfreeDx1carttomwc(double drcart[1+Nfree*D], double dxmwc[1+Nfree*D], 
		double rootM[1+N*D]){

  int i,j,k;
  for(j=1;j<=Nfree*D;j=j+1){
    dxmwc[j] = drcart[j]*rootM[j];
  }
}

void gcarttomwc(double Gcart[1+Nfree*D], double Gmwc[1+Nfree*D], 
		double rootM[1+N*D]){
  int i,j,k;
  for(j=1;j<=Nfree*D;j=j+1){
    Gmwc[j] = Gcart[j]/rootM[j];
  }
}

void hcarttomwc(double Hcart[1+Nfree*D][1+Nfree*D],
		double Hmwc[1+Nfree*D][1+Nfree*D], double rootM[1+N*D]){
 
  int i,j,k;
  
  for(j=1;j<=Nfree*D;j=j+1){
    for(k=1;k<=Nfree*D;k=k+1){
      Hmwc[j][k] = Hcart[j][k]/(rootM[j]*rootM[k]);
    }
  }
}

void NxDtoNDx1(double xyz[1+N][1+D], double x[1+N*D]){
  int i, j;
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      x[(i-1)*D+j] = xyz[i][j];
    }
  }/*
  if(DEBUG == 1){
    printf("\n  NxDtoNDx1: ");
    printstate(xyz);
    printf("\n  ");
    for(j=1;j<=N*D;j=j+1){
      printf(" %.2f",x[j]);
    }
    }*/
}

void NDx1toNxD(double x[1+N*D], double xyz[1+N][1+D]){
  int i, j;
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      xyz[i][j] = x[(i-1)*D+j];
    }
  }/*
  if(DEBUG == 1){
    printf("\n  NDx1toNxD: ");
    for(j=1;j<=N*D;j=j+1){
      printf(" %.2f",x[j]);
    }
    printstate(xyz);
    }*/
  
}

void NfreexDtoNfreeDx1(double xyz[1+Nfree][1+D], double x[1+Nfree*D]){
  int i, j;
  for(i=1;i<=Nfree;i=i+1){
    for(j=1;j<=D;j=j+1){
      x[(i-1)*D+j] = xyz[i][j];
    }
  }/*
  if(DEBUG == 1){
    printf("\n  NfreexDtoNfreeDx1: ");
    printstate(xyz);
    printf("\n  ");
    for(j=1;j<=Nfree*D;j=j+1){
      printf(" %.2f",x[j]);
    }
    }*/
}

void NfreeDx1toNfreexD(double x[1+Nfree*D], double xyz[1+Nfree][1+D]){
  int i, j;
  for(i=1;i<=Nfree;i=i+1){
    for(j=1;j<=D;j=j+1){
      xyz[i][j] = x[(i-1)*D+j];
    }
  }/*
  if(DEBUG == 1){
    printf("\n  NfreeDx1toNfreexD: ");
    for(j=1;j<=Nfree*D;j=j+1){
      printf(" %.2f",x[j]);
    }
    printstate(xyz);
    }*/
  
}

