void center_of_mass(double xyzB[1+N][1+D]){
  int i,j,k;
  double temp, com[1+D];  
  
  if(N == Nfree && N != 1){
    temp = 0.0;
    for(k=1;k<=D;k=k+1){
      com[k] = 0.0;
    }
    for(j=1;j<=N;j=j+1){
      temp = temp + (double)Mass[j];
      for(k=1;k<=D;k=k+1){
        com[k] = com[k] + ((double)Mass[j])*xyzB[j][k];
      }
    }
    for(k=1;k<=D;k=k+1){
      com[k] = com[k]/temp;
    }
    for(j=1;j<=N;j=j+1){
      for(k=1;k<=D;k=k+1){
        xyzB[j][k] = xyzB[j][k] - com[k];
      }
    }
    printf("\n  center of mass was (%.3f, %.3f, %.3f)",
  	   com[1],com[2],com[3]);
  }
}

void writeeulermatrix(double phi   /* 1st about z */,
		      double theta /* 2nd about y */,
		      double psi   /* 3rd @ new z */,
		      double eulermatrix[1+D][1+D]){
		      
  eulermatrix[1][1] = cos(phi)*cos(theta)*cos(psi)-sin(phi)*sin(psi);
  eulermatrix[1][2] = -sin(phi)*cos(theta)*cos(psi)-cos(phi)*sin(psi);
  eulermatrix[1][3] = sin(theta)*cos(psi);
  eulermatrix[2][1] = sin(phi)*cos(psi)+cos(phi)*cos(theta)*sin(psi);
  eulermatrix[2][2] = -sin(phi)*cos(theta)*sin(psi)+cos(phi)*cos(psi);
  eulermatrix[2][3] = sin(theta)*sin(psi);
  eulermatrix[3][1] = -cos(phi)*sin(theta);
  eulermatrix[3][2] = sin(theta)*sin(phi);
  eulermatrix[3][3] = cos(theta);
}

double rotateddistance2(double xyzref[1+N][1+D], 
			double eulermatrix[1+D][1+D],
			double xyz[1+N][1+D]){
  double xyzrotated[1+N][1+D];
  double differenceND[1+N*D];
  double dist2;
  int i,j,k;
  
  for(j=1;j<=N;j=j+1){
    DxDdotDx1(eulermatrix,xyz[j],xyzrotated[j]);
  }
  for(j=1;j<=N;j=j+1){
    for(k=1;k<=D;k=k+1){
      differenceND[(j-1)*D+k] = xyzrotated[j][k]-xyzref[j][k];
    }
  }
  dist2 = dotND(differenceND,differenceND);
  return dist2;
}

double optimaleulerangles(double xyzref[1+N][1+D], 
			  double xyzdisp[1+N][1+D],
			  double phitpsi[1+3]){

  double dist2,graddist2[1+3],temp,phi,theta,psi;
  double phi0,psi0,theta0,angle0,gridsize,initialgrid;
  double eulermatrix[1+D][1+D],transposerow[1+D];
  double eulermatrix1[1+D][1+D],eulermatrix2[1+D][1+D];
  double anglegradient[1+D],unitgradient[1+D],anglechanges[1+D];
  double dddphi,dddtheta,dddpsi,curvature,twokp1;
  int i,j,k,num,nutt,iterations,kk,jj,k_;

  /***   -2pi          .|   |   |   0   |   |   |.          2pi    ***/
  /***   -2pi          . |||||||    0            .          2pi    ***/

  k_ = 4;
  twokp1 = (double)k_*2.0+1.0;
  iterations = 2;    
  initialgrid = 2.0*3.14159/twokp1;
  gridsize = initialgrid;
  
  phi0 = theta0 = psi0 = 0;
  phitpsi[1] = 0;
  phitpsi[2] = 0;
  phitpsi[3] = 0;
  writeeulermatrix(phi0,theta0,psi0,eulermatrix);
  temp = rotateddistance2(xyzref,eulermatrix,xyzdisp);
                                     /* starting squared dist  */

  for(nutt=0;nutt<=iterations;nutt=nutt+1){
    for(i=-k_;i<=k_;i=i+1){
      phi = phi0+((double)i)*gridsize;
      for(j=-k_;j<=k_;j=j+1){
	theta = theta0+((double)j)*gridsize;
	for(k=-k_;k<=k_;k=k+1){
	  psi = psi0+((double)k)*gridsize;
	  if(k!=0 && j!=0 && i!=0){
	    writeeulermatrix(phi,theta,psi,eulermatrix);
	    dist2 = rotateddistance2(xyzref,eulermatrix,xyzdisp);
	    if(dist2 < temp){
	      temp = dist2;
	      phitpsi[1] = phi;
	      phitpsi[2] = theta;
	      phitpsi[3] = psi;
	      printf("\n  [d2 = %.2f]", dist2);
	      printf(" angles: [%.2f, %.2f, %.2f]",phi, theta, psi);
	      for(jj=1;jj<=D;jj=jj+1){
		printf("  ");
		for(kk=1;kk<=D;kk=kk+1){
		  printf("%.1f ",dot(eulermatrix[jj],eulermatrix[kk]));
		}
	      }
	    }
	  }
	}
      }
    }
    phi0 = phitpsi[1];
    theta0 = phitpsi[2];
    psi0 = phitpsi[3];
    gridsize = gridsize/((double)k_+1.0);
  }

  gridsize = .001;

  /* now use Newton-Raphson steps */
  printf("\n  SWITCHING TO NEWTON RAPHSON NOW...");

  for(k=1;k<=7;k=k+1){
    
    writeeulermatrix(phi0+gridsize,theta0,psi0,eulermatrix1);
    writeeulermatrix(phi0-gridsize,theta0,psi0,eulermatrix2);
    dddphi = (rotateddistance2(xyzref,eulermatrix1,xyzdisp)-
	      rotateddistance2(xyzref,eulermatrix2,xyzdisp))/(2.0*gridsize);
    
    writeeulermatrix(phi0,theta0+gridsize,psi0,eulermatrix1);
    writeeulermatrix(phi0,theta0-gridsize,psi0,eulermatrix2);
    dddtheta = (rotateddistance2(xyzref,eulermatrix1,xyzdisp)-
		rotateddistance2(xyzref,eulermatrix2,xyzdisp))
      /(2.0*gridsize);
    
    writeeulermatrix(phi0,theta0,psi0+gridsize,eulermatrix1);
    writeeulermatrix(phi0,theta0,psi0-gridsize,eulermatrix2);
    dddpsi = (rotateddistance2(xyzref,eulermatrix1,xyzdisp)-
	      rotateddistance2(xyzref,eulermatrix2,xyzdisp))/(2.0*gridsize);
    
    anglegradient[1] = dddphi;
    anglegradient[2] = dddtheta;
    anglegradient[3] = dddpsi;
    
    for(i=1;i<=3;i=i+1){
      unitgradient[i] = 
	anglegradient[i]/sqrt(dot(anglegradient,anglegradient));
    }

    writeeulermatrix(phi0+unitgradient[1]*gridsize,
		     theta0+unitgradient[2]*gridsize,
		     psi0+unitgradient[3]*gridsize,eulermatrix1);
    writeeulermatrix(phi0-unitgradient[1]*gridsize,
		     theta0-unitgradient[2]*gridsize,
		     psi0-unitgradient[3]*gridsize,eulermatrix2);
    /* still have old square dist. in temp */
    
    curvature = (rotateddistance2(xyzref,eulermatrix1,xyzdisp)+
		 rotateddistance2(xyzref,eulermatrix2,xyzdisp)-
		 2.0*temp)/(gridsize*gridsize);
    
    for(i=1;i<=D;i=i+1){
      anglechanges[i] = -anglegradient[i]/curvature;
    }
    
    /* add angle changes to current angles */
    
    phi0 = phitpsi[1] = phi0 + anglechanges[1];
    theta0 = phitpsi[2] = theta0 + anglechanges[2];
    psi0 = phitpsi[3] = psi0 + anglechanges[3];
    
    writeeulermatrix(phi0,theta0,psi0,eulermatrix);
    temp = rotateddistance2(xyzref,eulermatrix,xyzdisp);
    
  }
  printf("\n  d(d2)/d(phi,theta,psi) = (%.3f, %.3f, %.3f)",
	 anglegradient[1], anglegradient[2], anglegradient[3]);
  return temp;
}

void rotate_NxD(double eulermatrix[1+3][1+3],
	       double v[1+N][1+D],
	       double v_rot[1+N][1+D]){
	       
  int i,j,k;
  double v3[1+3];
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=D;j=j+1){
      v3[j] = v[i][j];
    }
    DxDdotDx1(eulermatrix, v3, v3);
    for(j=1;j<=D;j=j+1){
      v_rot[i][j] = v3[j];
    }
  }
}

void rotate_NfreeD(double eulermatrix[1+3][1+3],
	       double vNfreeD[1+Nfree*D],
	       double vrotNfreeD[1+Nfree*D]){
  int i,j,k;
  double v3[1+3], v[1+Nfree][1+D], v_rot[1+Nfree][1+D];
  
  NfreeDx1toNfreexD(vNfreeD, v);
  
  for(i=1;i<=Nfree;i=i+1){
    for(j=1;j<=D;j=j+1){
      v3[j] = v[i][j];
    }
    DxDdotDx1(eulermatrix, v3, v3);
    for(j=1;j<=D;j=j+1){
      v_rot[i][j] = v3[j];
    }
  }
  
  NfreexDtoNfreeDx1(v_rot, vrotNfreeD);
}

void inertia_tensor(double xyzM[1+N*D], double I[1+3][1+3]){
  int j, k;
  double temp;

  for(j=1;j<=3;j=j+1){
    for(k=1;k<=3;k=k+1){
      I[j][k] = 0.0;
    }
  }

  for(j=1;j<=N;j=j+1){

    I[1][1] = I[1][1] 
      + pow(xyzM[(j-1)*D+2],2.0) 
      + pow(xyzM[(j-1)*D+3],2.0);
    I[2][2] = I[2][2] 
      + pow(xyzM[(j-1)*D+1],2.0) 
      + pow(xyzM[(j-1)*D+3],2.0);
    I[3][3] = I[3][3] 
      + pow(xyzM[(j-1)*D+1],2.0) 
      + pow(xyzM[(j-1)*D+2],2.0);

    I[1][2] = I[1][2] 
      - xyzM[(j-1)*D+1]*xyzM[(j-1)*D+2];
    I[1][3] = I[1][3] 
      - xyzM[(j-1)*D+1]*xyzM[(j-1)*D+3];
    I[2][3] = I[2][3] 
      - xyzM[(j-1)*D+2]*xyzM[(j-1)*D+3];
  }
  I[3][2] = I[2][3];
  I[2][1] = I[1][2];
  I[3][1] = I[1][3];
}

