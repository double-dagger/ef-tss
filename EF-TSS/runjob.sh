#!/bin/bash
### time limit hr:min:sec requests 200 hours
#SBATCH --time=1000:00:00
### Writes standard input and output to a file with the name of $PBS_JOBID
#SBATCH -o test

### Job name
mkdir $JOBPATH/scratch
TMPDIR=$JOBPATH/scratch
cp $SLURM_SUBMIT_DIR/* $TMPDIR
cd $TMPDIR

export g16root="/share/apps"
export GAUSS_SCRDIR=$TMPDIR
. $g16root/g16/bsd/g16.profile

./bpef_from_hess.out > tss.txt
ls
# cp * $PBS_O_WORKDIR
mkdir $SLURM_SUBMIT_DIR/g16-logs
cp -p hessian grad $SLURM_SUBMIT_DIR/g16-logs
cp -p convergence.txt ef_movie.xyz tsfile.xyz tss.txt $SLURM_SUBMIT_DIR/

rm -r $TMPDIR

