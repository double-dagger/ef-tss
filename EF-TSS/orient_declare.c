void center_of_mass(double [1+N][1+D]);
void center_of_position(double [1+N][1+D]);

void matrixdotvD(double [1+D][1+D], double [D+1], double [D+1]);

void writeeulermatrix(double,
		      double,
		      double,
		      double [1+D][1+D]);

double rotateddistance2(double [1+N][1+D], 
			double [1+D][1+D],
			double [1+N][1+D]);

double optimaleulerangles(double [1+N][1+D], 
			  double [1+N][1+D],
			  double [1+3]);

void rotate_NxD(double [1+3][1+3],
	       double [1+N][1+D],
		double [1+N][1+D]);

void rotate_NfreeD(double [1+3][1+3],
	       double [1+Nfree*D],
	       double [1+Nfree*D]);

void inertia_tensor(double [1+N*D], double [1+3][1+3]);

