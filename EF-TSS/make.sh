#!/bin/bash
####################################################################################################################
#This script can help you get the d-band center value from gdosP-up/dn-d.dat(if ISPIN=2) and gdosP-d.dat(if ISPIN=1)
###################################
#By xijun Wang from USTC /2/25/2016
#E-mail:wangxijun1016@gmail.com
#QQ:710340788
####################################################################################################################

BASE_DIR=$1
echo $BASE_DIR
#check files
if [ ! -f "${BASE_DIR}/input.xyz" ]; then
echo "Error! There is no input.xyz file!\n" 
fi
#count atom numbers
atom_number=`sed -n "1 p" ${BASE_DIR}/input.xyz`

#get data
awk '{print $1}' ${BASE_DIR}/input.xyz > elements.tmp
awk '{printf "%s    %s    %s\n",$2,$3,$4}' ${BASE_DIR}/input.xyz > coordinates.tmp
sed -i '1,2 d' elements.tmp
sed -i '1 d' coordinates.tmp
#transfer element symbols to order numbers
sed -i 's/Ra/88/g' elements.tmp;sed -i 's/Fr/87/g' elements.tmp;sed -i 's/Rn/86/g' elements.tmp;sed -i 's/At/85/g' elements.tmp;sed -i 's/Po/84/g' elements.tmp
sed -i 's/Bi/83/g' elements.tmp;sed -i 's/Pb/82/g' elements.tmp;sed -i 's/Tl/81/g' elements.tmp;sed -i 's/Hg/80/g' elements.tmp;sed -i 's/Au/79/g' elements.tmp
sed -i 's/Pt/78/g' elements.tmp;sed -i 's/Ir/77/g' elements.tmp;sed -i 's/Os/76/g' elements.tmp;sed -i 's/Re/75/g' elements.tmp;sed -i 's/W/74/g' elements.tmp
sed -i 's/Ta/73/g' elements.tmp;sed -i 's/Hf/72/g' elements.tmp;sed -i 's/Lu/71/g' elements.tmp;sed -i 's/Ba/56/g' elements.tmp;sed -i 's/Cs/55/g' elements.tmp
sed -i 's/Xe/54/g' elements.tmp;sed -i 's/I/53/g' elements.tmp;sed -i 's/Te/52/g' elements.tmp;sed -i 's/Sb/51/g' elements.tmp;sed -i 's/Sn/50/g' elements.tmp
sed -i 's/In/49/g' elements.tmp;sed -i 's/cd/48/g' elements.tmp;sed -i 's/Ag/47/g' elements.tmp;sed -i 's/Pd/46/g' elements.tmp;sed -i 's/Rh/45/g' elements.tmp
sed -i 's/Ru/44/g' elements.tmp;sed -i 's/Tc/43/g' elements.tmp;sed -i 's/Mo/42/g' elements.tmp;sed -i 's/Nb/41/g' elements.tmp;sed -i 's/Zr/40/g' elements.tmp
sed -i 's/Y/39/g' elements.tmp;sed -i 's/Sr/38/g' elements.tmp;sed -i 's/Rb/37/g' elements.tmp;sed -i 's/Kr/36/g' elements.tmp;sed -i 's/Br/35/g' elements.tmp
sed -i 's/Se/34/g' elements.tmp;sed -i 's/As/33/g' elements.tmp;sed -i 's/Ge/32/g' elements.tmp;sed -i 's/Ga/31/g' elements.tmp;sed -i 's/Zn/30/g' elements.tmp
sed -i 's/Cu/29/g' elements.tmp;sed -i 's/Ni/28/g' elements.tmp;sed -i 's/Co/27/g' elements.tmp;sed -i 's/Fe/26/g' elements.tmp;sed -i 's/Mn/25/g' elements.tmp
sed -i 's/Cr/24/g' elements.tmp;sed -i 's/V/23/g' elements.tmp;sed -i 's/Ti/22/g' elements.tmp;sed -i 's/Sc/21/g' elements.tmp;sed -i 's/Ca/20/g' elements.tmp
sed -i 's/K/19/g' elements.tmp;sed -i 's/Ar/18/g' elements.tmp;sed -i 's/Cl/17/g' elements.tmp;sed -i 's/Si/14/g' elements.tmp;sed -i 's/P/15/g' elements.tmp
sed -i 's/S/16/g' elements.tmp;sed -i 's/Al/13/g' elements.tmp;sed -i 's/Mg/12/g' elements.tmp;sed -i 's/Na/11/g' elements.tmp;sed -i 's/Ne/10/g' elements.tmp
sed -i 's/F/9/g' elements.tmp;sed -i 's/O/8/g' elements.tmp;sed -i 's/N/7/g' elements.tmp;sed -i 's/C/6/g' elements.tmp;sed -i 's/Be/4/g' elements.tmp
sed -i 's/B/5/g' elements.tmp;sed -i 's/Li/3/g' elements.tmp;sed -i 's/He/2/g' elements.tmp;sed -i 's/H/1/g' elements.tmp
#elements line to column ----------- line1
awk '{printf "%s ",$1}' elements.tmp > elements_line1.tmp
#111 ---------- line2
for((i=1;i<=atom_number;i++))
do
echo "1" >> 111_line2_1.tmp
done
awk '{printf "%d ",$1}' 111_line2_1.tmp > 111_line2_2.tmp
#merger line1 line2 coordinates
paste -s elements_line1.tmp 111_line2_2.tmp > ${BASE_DIR}/nearsaddleA
cat coordinates.tmp >> ${BASE_DIR}/nearsaddleA
#clean tmp files
for tmps in elements coordinates elements_line1 111_line2_1 111_line2_2
do
rm -f ${tmps}.tmp
done


