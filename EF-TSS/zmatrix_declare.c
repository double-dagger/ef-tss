void distances(double [1+N][1+N][1+D], double [1+N][1+N]);
void printdistances(double [1+N][1+N], double);
void bondvectors(double [1+N][1+N][1+D], double [1+N][1+D]);
double bondangle_degrees(double [1+N][1+D], int, int, int);
double dihedral_degrees(double [1+N][1+D], int, int, int, int);

