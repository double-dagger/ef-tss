void bondvectors(double vIJ[1+N][1+N][1+D], double xyzB[1+N][1+D]){
  int i,j,k;
  double temp;
  /* vIJ is vector from I to J */

  for(i=1;i<=N;i=i+1){
    for(j=1;j<=N;j=j+1){
      for(k=1;k<=D;k=k+1){
	vIJ[i][j][k] = xyzB[j][k]-xyzB[i][k];
      }
    }
  }
}

void printdistances(double dist[1+N][1+N], double cutoff){
  int i,j,k;
  double temp;
  printf("\n  BOND DISTANCES:");
  for(i=1;i<=N;i=i+1){
    printf("\n ");
    for(j=1;j<=N;j=j+1){
      if(dist[i][j] < cutoff){
	printf(" %.2f", dist[i][j]);
      }
      else{
	printf("     ");
      }
    }
  }
  printf("\n");
}

void distances(double vIJ[1+N][1+N][1+D], double dist[1+N][1+N]){
  int i,j,k;
  double temp;
  
  for(i=1;i<=N;i=i+1){
    for(j=1;j<=N;j=j+1){
      temp = 0.0;
      for(k=1;k<=D;k=k+1){
	temp = temp+vIJ[i][j][k]*vIJ[i][j][k];
      }
      dist[i][j] = sqrt(temp);
    }
  }
}
      
double dihedral_degrees(double xyz[1+N][1+D], 
	int a, int b, int c, int d){
	
	int i, j;
	double v[1+D], u[1+D], w[1+D];
	double uv, uw, uu, cost, vv, ww, t, convert;

	convert = 180.0/3.14159;

	for(i=1;i<=D;i=i+1){
		v[i] = xyz[a][i]-xyz[b][i];
		u[i] = xyz[c][i]-xyz[b][i];
		w[i] = xyz[d][i]-xyz[c][i];
	}
	uv = dot(u,v);
	uw = dot(u,w);
	uu = dot(u,u);
	for(i=1;i<=D;i=i+1){
		v[i] = v[i] - (uv/uu)*u[i];
		w[i] = w[i] - (uw/uu)*u[i];
	}
	vv = dot(v,v);
	ww = dot(w,w);
	cost = dot(v, w)/sqrt(vv*ww);
	t = acos(cost);
	return (convert*t);
}

double bondangle_degrees(double xyz[1+N][1+D], 
	int a, int b, int c){
	
	int i, j;
	double v[1+D], u[1+D];
	double uv, uu, vv, cost, t, convert;

	convert = 180.0/3.14159;

	for(i=1;i<=D;i=i+1){
		v[i] = xyz[a][i]-xyz[b][i];
		u[i] = xyz[c][i]-xyz[b][i];
	}
	uv = dot(u,v);
	uu = dot(u,u);
	vv = dot(v,v);
	cost = uv/sqrt(uu*vv);
	t = acos(cost);
	return (convert*t);
}

