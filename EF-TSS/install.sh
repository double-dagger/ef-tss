#!/bin/bash
###
# Script to add an environment variable containing `EF-TSS` directory
###

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR="${DIR%x}"


if [ ${EFTSS_DIR+x} ]
then
    echo "EF-TSS environment variable already exists"
    echo "Value is ${EFTSS_DIR}"
else
    echo '# EF-TSS directory' >> ${HOME}/.bashrc
    echo "export EFTSS_DIR='$DIR'" >> ${HOME}/.bashrc
    echo "EF-TSS evironment variable added!"
    
    echo "# EF-TSS job sumbission alias" >> ${HOME}/.bashrc
    echo "alias subTSS='python '$DIR'/input.py'" >> ${HOME}/.bashrc
    echo "EF-TSS job sumbission alias added!"

    source ~/.bashrc
    echo "${HOME}/.bashrc updated"
fi





