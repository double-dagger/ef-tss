# Eigenvector Following Transition State Search (EF-TSS) Algorithm

C-code implementing an eigenvector following (EF) algorithm to locate transition states (TS). The EF-TSS algorithm also allows positions of certain atoms to be fixed, which is useful for calculations on cluster models of solid catalysts. All the EF algorithm stuff is the C code written by Baron. Energy, gradient, and Hessian calculations are done by Gaussian16. In general, Baron's EF algorithm locates TSs much more reliably than Gaussian's TS search algorithms. This repo also contains some additional scripts, written by Xijun and Craig, which makes creating and submitting jobs *significantly* easier than the original code. The documentation here will focus on how to actually use this code and will not discuss the EF algorithm. 

## Prerequisites and Installation

This repo requires Python (only standard libraries are used) and Gaussian 16 to be installed. The GCC compiler is also called. Jobs are submitted using Slurm. If this is ran on Bsharri, all of these pre-requisites are met (as long as you can run Gaussian 16 calculations).

Installation requires you to create and environment variable of the installation path. An installation script will do this for you by running `./install.sh`.

Alternatively, you can manually add the following lines to your `.bashrc` file:

```sh
# EF-TSS directory
export EFTSS_DIR='/home/path/to/EF-TSS'

# EF-TSS job sumbission alias
alias subTSS='python $EFTSS_DIR/input.py'
```

## Creating and submitting a job

### Input files 

This section will show how to create the necessary input files and submit a job, and will follow the HCN isomerization example in `examples/`

Three files are required to submit a job: `input.xyz`, `input.json`, and `basis`. The file `input.xyz` contains the initial guess and has the form:

```
3

1       -1.118243759      0.270488165      0.000000000
7        0.080784000     -0.576195500      0.000000000
6        0.080784000      0.623804500      0.000000000
```

The 3 designates the number of atoms and the following values are the Cartesian coordinates. Additionally, any frozen atoms are to be placed at the bottom. In this example, the N and C atoms are frozen, so they go below the H atom entry.

Next, the input file must be made, `input.json`. It follows the schema:

```JSON
{
    "N" : 3,
    "Nfree" : 1,
    "Charge": 0,
    "Spin": 1,
    "MEMORY": 15,
    "MAX_DIM_MULTIPLE": 10,
    "RESET_HESSIAN": 15,

    "name" : "HCN",
    "nprocs" : 24,
    "link-0": [],
    "route-append" : "wB97XD/TZVP"
}
```

The top group are parameters used by the EF-TSS algorithm, and will be passed as define directives via a preprocessor macro in `bpef_from_hess.c`. `N` is the number of atoms, `Nfree` is the number of atoms allowed to relax in the structure optimization, the C and N atoms are held fixed, so the 1 H atom can relax. `Spin` and `Charge` are the spin and charge of your system. IDK what `MEMORY` or `MAX_DIM_MULTIPLE` do, but the code works with those values and it has never not worked with those values so probably don't change them. `RESET_HESSIAN` is how many optimization iterations to perform before recalculating the Hessian.

The bottom group are other parameters not used for `bpef_from_hess.c`. `name` will be the name of the job when submitted via slurm. `nprocs` is how many cores to use in the calculation. `link-0` is an array and contains any link 0 command ***in addition*** to the number of cores. Lastly, `route-append` is any route card options to add to the gradient/Hessian calculations. This will typically be the XC functional and basis set. The route card for the gradient calculation contains `#p FORCE NOSYMM SCF=(MaxCycle=1000) ` and the route card for the Hessian calculation contains `#p FREQ NOSYMM PUNCH(DERIVATIVES) SCF=(MaxCycle=1000) `.

Finally, the `basis` file is needed. This is just a plain text file that contains more complex basis set definitions. This file would be used if you wanted to have different basis sets on different atoms or define a basis set (*e.g.*, one from basis set exchange). And example can be seen in `examples/oleifn-metathesis-cycloreversion`. It will probably be the most complex example: different basis sets are used for different atoms, a basis set for Re is defined, and an ECP for Re is used.

### Submitting a job

Once the files are created, `cd` to the directory containing the input files and enter the command `subTSS`. This will run the `input.py` script, which will perform the necessary file creation and compilation steps, then submit the job. 


## Job progress

A scratch subdirectory will be created, which is where the job will run. Progress can be monitored in `scratch/convergence.txt` and `scratch/tss.txt`. The `convergence.txt` file will give the number of iterations, norm of the gradient, and number of imaginary frequencies. The `tss.txt` file will give more detailed information. When the job is finished, the transition state structure will be in `tsfile.xyz`. A movie of the imaginary mode can be found in `ef_movie.xyz`

Log files for the initial Hessian calculation and latest gradient calculation will also be stored in `g16logs/`. Note the Hessian is updated with SR1 at each optimization step so the `Hessian` file only corresponds to the Hessian at multiples of `RESET_HESSIAN`.

## Computing free energies

The EF-TSS algorithm will give the transition state structure, but often we want the Gibbs free energy. This requires the exact Hessian, so that will need to be computed through a frequency calculation in Gaussian 16. 

One important note is if atoms are frozen through the `notatoms` command, you are required to redo the structure optimization. It is often beneficial to use a loose convergence criteria, `opt(loose)`. The forces are almost always much smaller than Gaussian's regular convergence criteria. However, theres been a couple cases where the `Displacement` values were not converged, and Gaussian's optimization algorithm never converged to the transition state structure (even though the initial guess was the transition state structure!). The `Displacement` convergence criteria depends on the optimization algorithm and is much less important than other requirements for transition states (1 imaginary mode and all other degrees of freedom have no forces acting upon them).